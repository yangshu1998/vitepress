import{_ as m,o as r,c as A,z as n,t as o,h as D,a2 as g,l as h,w as y,F as x,D as v,G as d,B as C,b as u,O as f}from"./chunks/framework.c9b07a32.js";const E={name:"vue2",data(){return{con:"你好世界"}},methods:{handleClick(){this.con="HelloWord"}}};function _(p,s,t,l,a,i){return r(),A("div",null,[n("h1",{onClick:s[0]||(s[0]=(...e)=>i.handleClick&&i.handleClick(...e))},o(a.con),1)])}const b=m(E,[["render",_]]),B=Object.freeze(Object.defineProperty({__proto__:null,default:b},Symbol.toStringTag,{value:"Module"})),k={setup(){const p=D("你好世界");return{con:p,handleClick:()=>{p.value="HelloWord"}}}};function q(p,s,t,l,a,i){return r(),A("div",null,[n("h1",{onClick:s[0]||(s[0]=(...e)=>l.handleClick&&l.handleClick(...e))},o(l.con),1)])}const V=m(k,[["render",q]]),F=Object.freeze(Object.defineProperty({__proto__:null,default:V},Symbol.toStringTag,{value:"Module"})),P={__name:"index4",setup(p){const s=D("你好世界"),t=()=>{s.value="HelloWord"};return(l,a)=>(r(),A("div",null,[n("h1",{onClick:t},o(s.value),1)]))}},w=Object.freeze(Object.defineProperty({__proto__:null,default:P},Symbol.toStringTag,{value:"Module"})),j={__name:"watch",setup(p){const s=D(1),t=D(2),l=g({name:"小白",age:"18"}),a=()=>{s.value++,l.name="小黑"},i=h(()=>s.value+t.value);return y(s,(e,c)=>{console.log(e,c)}),y([s,t],(e,c)=>{console.log(e,c)}),y(()=>l.name,(e,c)=>{console.log(e,c)},{deep:!0}),(e,c)=>(r(),A(x,null,[n("div",{onClick:a},o(s.value),1),n("h1",null,"count: "+o(s.value),1),n("h1",null,"count1: "+o(t.value),1),n("h1",null,o(i.value),1)],64))}},S=Object.freeze(Object.defineProperty({__proto__:null,default:j},Symbol.toStringTag,{value:"Module"})),T="/vitepress/assets/5.79f8f9db.png",$=f('<h1 id="vue3-与-vue2-的区别" tabindex="-1">Vue3 与 Vue2 的区别 <a class="header-anchor" href="#vue3-与-vue2-的区别" aria-label="Permalink to &quot;Vue3 与 Vue2 的区别&quot;">​</a></h1><h2 id="生命周期" tabindex="-1">生命周期 <a class="header-anchor" href="#生命周期" aria-label="Permalink to &quot;生命周期&quot;">​</a></h2><blockquote><p>Vue3 和 Vue2 生命周期基本相同，都可以通过配置项去使用生命周期钩子， 在流程上有一些许区别，一些钩子名称发生变化，并且Vue3可以通过组合式API形式 去使用生命组件钩子</p></blockquote><h3 id="vue2-生命周期图示-vue3-生命周期图示" tabindex="-1">Vue2 生命周期图示 Vue3 生命周期图示 <a class="header-anchor" href="#vue2-生命周期图示-vue3-生命周期图示" aria-label="Permalink to &quot;Vue2 生命周期图示  Vue3 生命周期图示&quot;">​</a></h3><p><img src="'+T+'" alt="生命周期" data-fancybox="gallery"></p><ol><li>Vue3 可以继续使用 Vue2中的生命周期钩子，但有两个被更名</li></ol><ul><li><code>beforeDestroy</code> --- <code>beforeUnmount</code></li><li><code>destroyed</code> --- <code>unmounted</code></li></ul><ol start="2"><li>从上图可以看出Vue3 先将<code>el</code>挂载之后在进行 <code>beforeCreate</code>，而Vue2是在 <code>created</code> 之后再进行判断是否挂载 <code>el</code> 如果没有，则流程终止，<code>beforeCreate</code>和 <code>created</code> 俩个钩子冗余使用，因此Vue3进行了优化的</li></ol><h3 id="组合式api形式使用生命周期钩子" tabindex="-1">组合式API形式使用生命周期钩子 <a class="header-anchor" href="#组合式api形式使用生命周期钩子" aria-label="Permalink to &quot;组合式API形式使用生命周期钩子&quot;">​</a></h3><table><thead><tr><th style="text-align:left;">Vue2</th><th style="text-align:left;">Vue3</th><th style="text-align:left;"></th></tr></thead><tbody><tr><td style="text-align:left;">beforeCreate</td><td style="text-align:left;">setup()</td><td style="text-align:left;"></td></tr><tr><td style="text-align:left;">created</td><td style="text-align:left;">setup()</td><td style="text-align:left;">开始创建组件之前，在beforeCreate和created之前执行。创建的是data和method</td></tr><tr><td style="text-align:left;">beforeMount</td><td style="text-align:left;">onBeforeMount</td><td style="text-align:left;">组件挂载到节点上之前执行的函数</td></tr><tr><td style="text-align:left;">mounted</td><td style="text-align:left;">onMounted</td><td style="text-align:left;">组件挂载完成后执行的函数</td></tr><tr><td style="text-align:left;">beforeUpdate</td><td style="text-align:left;">onBeforeUpdate</td><td style="text-align:left;">组件更新完成之后执行的函数</td></tr><tr><td style="text-align:left;">updated</td><td style="text-align:left;">onUpdated</td><td style="text-align:left;">组件更新完成之后执行的函数</td></tr><tr><td style="text-align:left;">beforeDestroy</td><td style="text-align:left;">onBeforeUnmount</td><td style="text-align:left;">组件卸载之前执行的函数</td></tr><tr><td style="text-align:left;">destroyed</td><td style="text-align:left;">onUnmounted</td><td style="text-align:left;">组件卸载完成后执行的函数</td></tr><tr><td style="text-align:left;">activated</td><td style="text-align:left;">onActivated</td><td style="text-align:left;">被包含在<code>&lt;keep-alive&gt;</code>中的组件，会多出两个生命周期钩子函数。被激活时执行</td></tr><tr><td style="text-align:left;">deactivated</td><td style="text-align:left;">onDeactivated</td><td style="text-align:left;">比如从 A 组件，切换到 B 组件，A 组件消失时执行</td></tr><tr><td style="text-align:left;">errorCaptured</td><td style="text-align:left;">onErrorCaptured</td><td style="text-align:left;">当捕获一个来自子孙组件的异常时激活钩子函数</td></tr></tbody></table><h2 id="选项式api-与-组合式-api" tabindex="-1">选项式API 与 组合式 API <a class="header-anchor" href="#选项式api-与-组合式-api" aria-label="Permalink to &quot;选项式API 与 组合式 API&quot;">​</a></h2><ul><li>选项式api</li></ul>',12),M=n("p",null,"index2/index2",-1),z=n("ul",null,[n("li",null,"组合式api")],-1),I=n("p",null,"index2/index3",-1),O=n("ul",null,[n("li",null,"setup 语法糖")],-1),H=n("p",null,"index2/index4",-1),W=f('<h3 id="总结" tabindex="-1">总结 <a class="header-anchor" href="#总结" aria-label="Permalink to &quot;总结&quot;">​</a></h3><ul><li>选项式api</li></ul><blockquote><p>vue2 的选项式api 在组织代码中，在一个vue文件中<code>data</code>,<code>methods</code>,<code>computed</code>,<code>watch</code>中定义属性和方法，共同处理页面逻辑</p><p>优点：新手上手简单</p><p>缺点：在复杂项目中，不同功能之前的方法、属性、计算属性都要对应去编写内容，查找相应内容时候还需要上下翻滚，影响效率</p></blockquote><p><img src="https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e256700683674f71978c9dc13e64ae8e~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image" alt="https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e256700683674f71978c9dc13e64ae8e~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image"></p><ul><li>组合式api</li></ul><blockquote><p>缺点：编写思维逻辑需要转变，学习成本增加</p><p>优点：<code>Composition API</code> 是根据逻辑相关组织相应代码 ， 可以提高相应代码可读性，复用性，基于函数组合的api更好的重用代码</p><p>在vue2中复用代码一般通过mixins 复用代码，但是容易导致命名冲突且关系不清楚</p><p><code>Composition API</code> 最大的优点可将一个功能相关的 放在同一个地方，可以将每个单独的功能放在独立的函数内，最后在组合起来</p></blockquote><p><img src="https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/eb5fcf366480466cab33f3efa7808a9a~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image" alt="https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/eb5fcf366480466cab33f3efa7808a9a~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image"></p><h2 id="声明响应式状态-ref-和-reactive" tabindex="-1">声明响应式状态（ref 和 reactive） <a class="header-anchor" href="#声明响应式状态-ref-和-reactive" aria-label="Permalink to &quot;声明响应式状态（ref 和 reactive）&quot;">​</a></h2><blockquote><p>在vue2中选项式api中，data中的函数都具有响应式，页面会随着data的数据变化而变化，而在vue3中没有data函数，为了解决这个问题引入了<code>ref</code> 和 <code>reactive</code> 函数使得变量变成响应式数据</p></blockquote><ul><li>ref</li></ul><p>1.ref的参数一般是基本数据类型，也可以是对象类型</p><p>2.如果参数是对象类型，本质的底层其实还是<code>reactive</code>，系统会自动将<code>ref</code>转换成<code>reactive</code>，例如<code>ref(1) ==&gt; reactive({value: 1})</code></p><p>3.在模板中访问<code>ref</code>的数据时，系统会自动添加<code>.value</code>，在Js中访问<code>ref</code>的数据，我们需要手动添加<code>.value</code></p><p>4.ref的底层原理同reactive一样，都是Proxy</p><ul><li>reactive</li></ul><p>1.reactive的参数一般是对象或者数组，它能够将复杂的数据类型转变为响应式数据</p><p>2.reactive的响应式是深层次的，底层本质是将传入的数据转换成Proxy对象</p><h2 id="watch-和-computed" tabindex="-1">watch 和 computed <a class="header-anchor" href="#watch-和-computed" aria-label="Permalink to &quot;watch 和 computed&quot;">​</a></h2><ul><li>watch函数</li><li>computed 函数</li></ul>',19),U=n("p",null,"index2/watch",-1),N=f(`<h2 id="vue3-组件通信" tabindex="-1">Vue3 组件通信 <a class="header-anchor" href="#vue3-组件通信" aria-label="Permalink to &quot;Vue3 组件通信&quot;">​</a></h2><table><thead><tr><th style="text-align:left;">方式</th><th style="text-align:left;">Vue2</th><th style="text-align:left;">Vue3</th></tr></thead><tbody><tr><td style="text-align:left;">父传子</td><td style="text-align:left;">props</td><td style="text-align:left;">props</td></tr><tr><td style="text-align:left;">子传父</td><td style="text-align:left;">$emit</td><td style="text-align:left;">emits</td></tr><tr><td style="text-align:left;">父传子</td><td style="text-align:left;">$attrs</td><td style="text-align:left;">attrs</td></tr><tr><td style="text-align:left;">子传父</td><td style="text-align:left;">$listeners</td><td style="text-align:left;">无(合并到 attrs方式)</td></tr><tr><td style="text-align:left;">父传子</td><td style="text-align:left;">provide</td><td style="text-align:left;">provide</td></tr><tr><td style="text-align:left;">子传父</td><td style="text-align:left;">inject</td><td style="text-align:left;">inject</td></tr><tr><td style="text-align:left;">子组件访问父组件</td><td style="text-align:left;">$parent</td><td style="text-align:left;">无</td></tr><tr><td style="text-align:left;">父组件访问子组件</td><td style="text-align:left;">$children</td><td style="text-align:left;">无</td></tr><tr><td style="text-align:left;">父组件访问子组件</td><td style="text-align:left;">$ref</td><td style="text-align:left;">expose&amp;ref</td></tr><tr><td style="text-align:left;">兄弟传值</td><td style="text-align:left;">EventBus</td><td style="text-align:left;">mitt</td></tr></tbody></table><h2 id="props" tabindex="-1">props <a class="header-anchor" href="#props" aria-label="Permalink to &quot;props&quot;">​</a></h2><p>props：父组件通过v-bind传入，子组件通过props接收</p><blockquote><p>注意：</p><p>props中的数据是单向的，即子组件不可更改父组件传过来的值</p><p>在组合式api中，如果想要在子组件中用其他变量接收props的值时需要使用toRef将props中的属性转化为响应式</p></blockquote><h2 id="emit" tabindex="-1">emit <a class="header-anchor" href="#emit" aria-label="Permalink to &quot;emit&quot;">​</a></h2><p>emit: 子组件可以通过emit发布一个事件并传递一些参数，父组件通过v-on进行这个事件的监听</p><h2 id="attrs和listeners" tabindex="-1">attrs和listeners <a class="header-anchor" href="#attrs和listeners" aria-label="Permalink to &quot;attrs和listeners&quot;">​</a></h2><ul><li>子组件使用$attrs可以获得父组件除了props传递的属性和特性绑定属性 (class和 style)之外的所有属性</li><li>子组件使用$listeners可以获得父组件(不含.native修饰器的)所有v-on事件监听器，在Vue3中已经不再使用；</li><li>但是Vue3中的attrs不仅可以获得父组件传来的属性也可以获得父组件v-on事件监听器</li></ul><h2 id="provide-inject" tabindex="-1">provide/inject <a class="header-anchor" href="#provide-inject" aria-label="Permalink to &quot;provide/inject&quot;">​</a></h2><ul><li><p>provide: 是一个对象，或者是返回一个对象的函数，里面包含要给子孙后代的东西，也就是属性和属性值</p></li><li><p>inject：一个字符串数组，或者是一个对象，获取父组件或者更高层次的组件provide的值，即在任何后代组件都可以通过inject获得</p></li></ul><h2 id="expose-ref" tabindex="-1">expose&amp;ref <a class="header-anchor" href="#expose-ref" aria-label="Permalink to &quot;expose&amp;ref&quot;">​</a></h2><ul><li>$refs可以直接获取元素属性，同时也可以直接获取子组件实例</li></ul><blockquote><p>注意：通过ref获取子组件实例必须在页面挂载完成后才能获取。</p><p>在使用setup语法糖时候，子组件必须元素或方法暴露出去父组件才能获取到</p></blockquote><h2 id="eventbus-mitt" tabindex="-1">EventBus/mitt <a class="header-anchor" href="#eventbus-mitt" aria-label="Permalink to &quot;EventBus/mitt&quot;">​</a></h2><ol><li><p>兄弟组件通信可以通过一个事件中心EventBus实现，即新建一个Vue实例来进行事件的监听，触发和销毁</p></li><li><p>在Vue3中没有了EventBus兄弟组件通信，但是现在有了一个替代的方案<code>mitt.js</code>, 原理还是EventBus</p></li></ol><p>首先先安装mitt</p><blockquote><p>npm i mitt -S</p></blockquote><p>新建文件src/utils/mitt.js</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import mitt from &#39;mitt&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">const Mitt = mitt()</span></span>
<span class="line"><span style="color:#A6ACCD;">export default Mitt</span></span></code></pre></div><ul><li>父组件</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">  &lt;Button type=&quot;primary&quot; @click=&quot;showConfirm&quot;&gt;弹窗&lt;/Button&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">  &lt;confirm ref=&quot;confirm&quot; text=&quot;是否清空所有搜索历史&quot; confirmBtnText=&quot;清空&quot; name=&#39;confirm&#39; @confirmClick=&#39;confirmClick&#39;&gt;&lt;/confirm&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">  &lt;hello-word&gt;&lt;/hello-word&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { Button } from &#39;vant&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">import Confirm from &#39;@/base/confirm.vue&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">import HelloWord from &#39;@/components/HelloWord.vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { ref, provide } from &#39;vue&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">const confirm = ref(null);</span></span>
<span class="line"><span style="color:#A6ACCD;">const msg = ref(&#39;下发给子组件&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">provide(&#39;msg&#39;, msg)</span></span>
<span class="line"><span style="color:#A6ACCD;">const showConfirm = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> confirm.value.show();</span></span>
<span class="line"><span style="color:#A6ACCD;">};</span></span>
<span class="line"><span style="color:#A6ACCD;">const confirmClick = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> console.log(&#39;父组件被调用&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;style lang=&quot;scss&quot;&gt;&lt;/style&gt;</span></span></code></pre></div><ul><li>子组件</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">  &lt;Button type=&quot;primary&quot; @click=&quot;handleClick&quot;&gt;调用父组件方法&lt;/Button&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">  &lt;Button type=&quot;primary&quot; @click=&quot;handleClickSendMsg&quot;&gt;传值&lt;/Button&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">  &lt;transition name=&quot;confirm-fade&quot;&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">   &lt;div class=&quot;confirm&quot; v-show=&quot;showFlag&quot;&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">    &lt;div class=&quot;confirm-wrapper&quot;&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">     &lt;div class=&quot;confirm-content&quot;&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">      &lt;p class=&quot;text&quot;&gt;{{props.text}}&lt;/p&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">      &lt;div class=&quot;operate&quot;&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">       &lt;div @click=&quot;cancel&quot; class=&quot;operate-btn left&quot;&gt;{{ props.cancelBtnText }}&lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">       &lt;div @click=&quot;confirm&quot; class=&quot;operate-btn&quot;&gt;{{ props.confirmBtnText }}&lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">      &lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">     &lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">    &lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">   &lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">  &lt;/transition&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { ref, useAttrs, inject } from &#39;vue&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { Button } from &#39;vant&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">import Mitt from &#39;@/utils/mitt.js&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">const showFlag = ref(false);</span></span>
<span class="line"><span style="color:#A6ACCD;">const attrs = useAttrs()</span></span>
<span class="line"><span style="color:#A6ACCD;">const emit = defineEmits([&#39;cancel&#39;, &#39;confirm&#39;])</span></span>
<span class="line"><span style="color:#A6ACCD;">const props = defineProps({</span></span>
<span class="line"><span style="color:#A6ACCD;"> text: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  type: String,</span></span>
<span class="line"><span style="color:#A6ACCD;">  default: &#39;&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"> },</span></span>
<span class="line"><span style="color:#A6ACCD;"> confirmBtnText: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  type: String,</span></span>
<span class="line"><span style="color:#A6ACCD;">  default: &#39;确定&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"> },</span></span>
<span class="line"><span style="color:#A6ACCD;"> cancelBtnText: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  type: String,</span></span>
<span class="line"><span style="color:#A6ACCD;">  default: &#39;取消&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">});</span></span>
<span class="line"><span style="color:#A6ACCD;">const show = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> showFlag.value = true</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">const hide = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> showFlag.value = false</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(attrs)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(inject(&#39;msg&#39;).value)</span></span>
<span class="line"><span style="color:#A6ACCD;">defineExpose({</span></span>
<span class="line"><span style="color:#A6ACCD;"> show,</span></span>
<span class="line"><span style="color:#A6ACCD;"> hide</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;">const cancel = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> hide()</span></span>
<span class="line"><span style="color:#A6ACCD;"> emit(&#39;cancel&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">};</span></span>
<span class="line"><span style="color:#A6ACCD;">const confirm = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> hide()</span></span>
<span class="line"><span style="color:#A6ACCD;"> emit(&#39;confirm&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">};</span></span>
<span class="line"><span style="color:#A6ACCD;">const handleClick = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> attrs.onConfirmClick(&#39;Child&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">const handleClickSendMsg = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> Mitt.emit(&#39;sendMsg&#39;, &#39;兄弟的值&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;style lang=&quot;scss&quot; scoped&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">.confirm {</span></span>
<span class="line"><span style="color:#A6ACCD;"> position: fixed;</span></span>
<span class="line"><span style="color:#A6ACCD;"> left: 0;</span></span>
<span class="line"><span style="color:#A6ACCD;"> right: 0;</span></span>
<span class="line"><span style="color:#A6ACCD;"> top: 0;</span></span>
<span class="line"><span style="color:#A6ACCD;"> bottom: 0;</span></span>
<span class="line"><span style="color:#A6ACCD;"> z-index: 998;</span></span>
<span class="line"><span style="color:#A6ACCD;"> background-color: rgba($color: #000000, $alpha: 0.3);</span></span>
<span class="line"><span style="color:#A6ACCD;"> &amp;.confirm-fade-enter-active {</span></span>
<span class="line"><span style="color:#A6ACCD;">  animation: confirm-fadein 0.3s;</span></span>
<span class="line"><span style="color:#A6ACCD;">  .confirm-content {</span></span>
<span class="line"><span style="color:#A6ACCD;">   animation: confirm-zoom 0.3s;</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;"> .confirm-wrapper {</span></span>
<span class="line"><span style="color:#A6ACCD;">  position: absolute;</span></span>
<span class="line"><span style="color:#A6ACCD;">  top: 50%;</span></span>
<span class="line"><span style="color:#A6ACCD;">  left: 50%;</span></span>
<span class="line"><span style="color:#A6ACCD;">  transform: translate(-50%, -50%);</span></span>
<span class="line"><span style="color:#A6ACCD;">  z-index: 999;</span></span>
<span class="line"><span style="color:#A6ACCD;">  .confirm-content {</span></span>
<span class="line"><span style="color:#A6ACCD;">   width: 270px;</span></span>
<span class="line"><span style="color:#A6ACCD;">   border-radius: 13px;</span></span>
<span class="line"><span style="color:#A6ACCD;">   background: #333;</span></span>
<span class="line"><span style="color:#A6ACCD;">   .text {</span></span>
<span class="line"><span style="color:#A6ACCD;">    padding: 19px 15px;</span></span>
<span class="line"><span style="color:#A6ACCD;">    line-height: 22px;</span></span>
<span class="line"><span style="color:#A6ACCD;">    text-align: center;</span></span>
<span class="line"><span style="color:#A6ACCD;">    font-size: 18px;</span></span>
<span class="line"><span style="color:#A6ACCD;">    color: rgba($color: #fff, $alpha: 0.5);</span></span>
<span class="line"><span style="color:#A6ACCD;">   }</span></span>
<span class="line"><span style="color:#A6ACCD;">   .operate {</span></span>
<span class="line"><span style="color:#A6ACCD;">    display: flex;</span></span>
<span class="line"><span style="color:#A6ACCD;">    align-items: center;</span></span>
<span class="line"><span style="color:#A6ACCD;">    text-align: center;</span></span>
<span class="line"><span style="color:#A6ACCD;">    font-size: 18px;</span></span>
<span class="line"><span style="color:#A6ACCD;">    .operate-btn {</span></span>
<span class="line"><span style="color:#A6ACCD;">     flex: 1;</span></span>
<span class="line"><span style="color:#A6ACCD;">     line-height: 22px;</span></span>
<span class="line"><span style="color:#A6ACCD;">     padding: 10px 0;</span></span>
<span class="line"><span style="color:#A6ACCD;">     border-top: 1px solid rgba($color: #000000, $alpha: 0.3);</span></span>
<span class="line"><span style="color:#A6ACCD;">     color: rgba($color: #fff, $alpha: 0.3);</span></span>
<span class="line"><span style="color:#A6ACCD;">     &amp;.left {</span></span>
<span class="line"><span style="color:#A6ACCD;">      border-right: 1px solid rgba($color: #000000, $alpha: 0.3);</span></span>
<span class="line"><span style="color:#A6ACCD;">     }</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">   }</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">@keyframes confirm-fadein {</span></span>
<span class="line"><span style="color:#A6ACCD;"> 0% {</span></span>
<span class="line"><span style="color:#A6ACCD;">  opacity: 0;</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;"> 100% {</span></span>
<span class="line"><span style="color:#A6ACCD;">  opacity: 1;</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">@keyframes confirm-zoom {</span></span>
<span class="line"><span style="color:#A6ACCD;"> 0% {</span></span>
<span class="line"><span style="color:#A6ACCD;">  transform: scale(0);</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;"> 50% {</span></span>
<span class="line"><span style="color:#A6ACCD;">  transform: scale(1.1);</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;"> 100% {</span></span>
<span class="line"><span style="color:#A6ACCD;">  transform: scale(1);</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/style&gt;</span></span></code></pre></div><ul><li>兄弟组件</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">   HelloWord</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;/div&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { onUnmounted } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import Mitt from &#39;@/utils/mitt.js&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">const getMsg = (val) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  console.log(val);//兄弟的值</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">Mitt.on(&#39;sendMsg&#39;, getMsg)</span></span>
<span class="line"><span style="color:#A6ACCD;">onUnmounted(() =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;"> //组件销毁 移除监听</span></span>
<span class="line"><span style="color:#A6ACCD;"> Mitt.off(&#39;sendMsg&#39;, getMsg)</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;style scoped&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/style&gt;</span></span></code></pre></div>`,26),G=JSON.parse('{"title":"Vue3 与 Vue2 的区别","description":"","frontmatter":{},"headers":[],"relativePath":"vue3/index2.md","filePath":"vue3/index2.md"}'),R={name:"vue3/index2.md"},K=Object.assign(R,{setup(p){const s=Object.assign({"../examples/index2/index2.vue":B,"../examples/index2/index3.vue":F,"../examples/index2/index4.vue":w,"../examples/index2/watch.vue":S});return(t,l)=>{const a=v("Demo");return r(),A("div",null,[$,d(a,{demos:u(s),path:"index2/index2",source:"%3Ctemplate%3E%0D%0A%20%20%3Cdiv%3E%0D%0A%20%20%20%20%3Ch1%20%40click%3D%22handleClick%22%3E%7B%7Bcon%7D%7D%3C%2Fh1%3E%0D%0A%20%20%3C%2Fdiv%3E%0D%0A%3C%2Ftemplate%3E%0D%0A%0D%0A%3Cscript%3E%0D%0A%20%20export%20default%20%7B%0D%0A%20%20%20%20name%3A%20'vue2'%2C%0D%0A%20%20%20%20data()%20%7B%0D%0A%20%20%20%20%20%20return%20%7B%0D%0A%20%20%20%20%20%20%20%20con%3A%20'%E4%BD%A0%E5%A5%BD%E4%B8%96%E7%95%8C'%0D%0A%20%20%20%20%20%20%7D%0D%0A%20%20%20%20%7D%2C%0D%0A%20%20%20%20methods%3A%20%7B%0D%0A%20%20%20%20%20%20handleClick()%20%7B%0D%0A%20%20%20%20%20%20%20%20this.con%20%3D%20'HelloWord'%0D%0A%20%20%20%20%20%20%7D%0D%0A%20%20%20%20%7D%0D%0A%20%20%7D%0D%0A%3C%2Fscript%3E%0D%0A%0D%0A%3Cstyle%20scoped%3E%0D%0A%0D%0A%3C%2Fstyle%3E"},{default:C(()=>[M]),_:1},8,["demos"]),z,d(a,{demos:u(s),path:"index2/index3",source:"%3Ctemplate%3E%0D%0A%20%20%3Cdiv%3E%0D%0A%20%20%20%20%3Ch1%20%40click%3D%22handleClick%22%3E%7B%7Bcon%7D%7D%3C%2Fh1%3E%0D%0A%20%20%3C%2Fdiv%3E%0D%0A%3C%2Ftemplate%3E%0D%0A%0D%0A%3Cscript%3E%0D%0Aimport%20%7B%20ref%20%7D%20from%20'vue'%0D%0Aexport%20default%20%7B%0D%0A%20%20setup%20()%20%7B%0D%0A%20%20%20%20const%20con%20%3D%20ref('%E4%BD%A0%E5%A5%BD%E4%B8%96%E7%95%8C')%0D%0A%20%20%20%20const%20handleClick%20%3D%20()%20%3D%3E%20%7B%0D%0A%20%20%20%20%20%20con.value%20%3D%20'HelloWord'%0D%0A%20%20%20%20%7D%0D%0A%20%20%20%20return%20%7B%0D%0A%20%20%20%20%20%20con%2C%0D%0A%20%20%20%20%20%20handleClick%0D%0A%20%20%20%20%7D%0D%0A%20%20%7D%0D%0A%7D%0D%0A%3C%2Fscript%3E%0D%0A%0D%0A%3Cstyle%20scoped%3E%0D%0A%0D%0A%3C%2Fstyle%3E"},{default:C(()=>[I]),_:1},8,["demos"]),O,d(a,{demos:u(s),path:"index2/index4",source:"%3Ctemplate%3E%0D%0A%20%20%3Cdiv%3E%0D%0A%20%20%20%20%3Ch1%20%40click%3D%22handleClick%22%3E%7B%7Bcon%7D%7D%3C%2Fh1%3E%0D%0A%20%20%3C%2Fdiv%3E%0D%0A%3C%2Ftemplate%3E%0D%0A%0D%0A%3Cscript%20setup%3E%0D%0Aimport%20%7B%20ref%20%7D%20from%20'vue'%3B%0D%0Aconst%20con%20%3D%20ref('%E4%BD%A0%E5%A5%BD%E4%B8%96%E7%95%8C')%0D%0Aconst%20handleClick%20%3D%20()%20%3D%3E%20%7B%0D%0A%20%20con.value%20%3D%20'HelloWord'%0D%0A%7D%0D%0A%3C%2Fscript%3E%0D%0A%0D%0A%3Cstyle%20scoped%3E%0D%0A%0D%0A%3C%2Fstyle%3E"},{default:C(()=>[H]),_:1},8,["demos"]),W,d(a,{demos:u(s),path:"index2/watch",source:"%3Ctemplate%3E%0D%0A%20%20%3Cdiv%20%40click%3D%22handleClick%22%3E%7B%7Bcount%7D%7D%3C%2Fdiv%3E%0D%0A%20%20%3Ch1%3Ecount%3A%20%7B%7Bcount%7D%7D%3C%2Fh1%3E%0D%0A%20%20%3Ch1%3Ecount1%3A%20%7B%7Bcount1%7D%7D%3C%2Fh1%3E%0D%0A%20%20%3Ch1%3E%7B%7BaddSum%7D%7D%3C%2Fh1%3E%0D%0A%3C%2Ftemplate%3E%0D%0A%0D%0A%3Cscript%20setup%3E%0D%0Aimport%20%7B%20ref%2C%20watch%2C%20reactive%2C%20computed%20%7D%20from%20'vue'%3B%0D%0Aconst%20count%20%3D%20ref(1)%0D%0Aconst%20count1%20%3D%20ref(2)%0D%0Aconst%20person%20%3D%20reactive(%7B%0D%0A%20%20name%3A%20'%E5%B0%8F%E7%99%BD'%2C%0D%0A%20%20age%3A%20'18'%0D%0A%7D)%0D%0Aconst%20handleClick%20%3D%20()%20%3D%3E%20%7B%0D%0A%20%20count.value%20%2B%2B%0D%0A%20%20person.name%20%3D%20'%E5%B0%8F%E9%BB%91'%0D%0A%7D%0D%0Aconst%20addSum%20%3D%20computed(()%20%3D%3E%20%7B%0D%0A%20%20return%20count.value%20%2B%20count1.value%0D%0A%7D)%0D%0A%2F%2F%20%E5%8D%95%E4%B8%AA%E7%9B%91%E5%90%AC%E6%95%B0%E6%8D%AE%0D%0Awatch(count%2C%20(newValue%2C%20oldValue)%20%3D%3E%20%7B%0D%0A%20%20console.log(newValue%2C%20oldValue)%0D%0A%7D)%0D%0A%0D%0A%2F%2F%20%E5%A4%9A%E4%B8%AA%E7%9B%91%E5%90%AC%E6%95%B0%E6%8D%AE%0D%0Awatch(%5Bcount%2C%20count1%5D%2C%20(newValue%2C%20oldValue)%20%3D%3E%20%7B%0D%0A%20%20console.log(newValue%2C%20oldValue)%20%2F%2F%20%E8%BE%93%E5%87%BA%E7%9A%84%E5%80%BC%E4%B8%BA%E6%95%B0%E7%BB%84%E6%A0%BC%E5%BC%8F%0D%0A%7D)%0D%0A%0D%0A%2F%2F%20%E7%9B%91%E5%90%AC%20reactive%20%E5%AE%9A%E4%B9%89%E7%9A%84%E5%93%8D%E5%BA%94%E5%BC%8F%E6%95%B0%E6%8D%AE%0D%0A%2F%2F%20%E5%BD%93%E7%9B%91%E5%90%AC%E4%B8%80%E4%B8%AA%E5%A4%8D%E6%9D%82%E5%AF%B9%E8%B1%A1%E7%9A%84%E5%B1%9E%E6%80%A7%E6%88%96%E8%80%85%E6%95%B0%E7%BB%84%E6%97%B6%E9%9C%80%E8%A6%81%E4%BC%A0%E5%85%A5%E7%AC%AC%E4%B8%89%E4%B8%AA%E5%8F%82%E6%95%B0%7Bdeep%3Atrue%7D%0D%0Awatch(()%20%3D%3E%20person.name%2C%20(newValue%2C%20oldValue)%20%3D%3E%20%7B%0D%0A%20%20console.log(newValue%2C%20oldValue)%0D%0A%7D%2C%20%7B%0D%0A%20%20deep%3A%20true%0D%0A%7D)%0D%0A%3C%2Fscript%3E%0D%0A%0D%0A%3Cstyle%20scoped%3E%0D%0A%0D%0A%3C%2Fstyle%3E"},{default:C(()=>[U]),_:1},8,["demos"]),N])}}});export{G as __pageData,K as default};
