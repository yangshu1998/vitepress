import{o as s,c as n,O as a}from"./chunks/framework.c9b07a32.js";const p=a(`<h1 id="proxy-object-defineproperty-vue双向绑定的原理" tabindex="-1">proxy -- Object.defineProperty() Vue双向绑定的原理 <a class="header-anchor" href="#proxy-object-defineproperty-vue双向绑定的原理" aria-label="Permalink to &quot;proxy -- Object.defineProperty() Vue双向绑定的原理&quot;">​</a></h1><h2 id="object-defineproperty" tabindex="-1">Object.defineProperty() <a class="header-anchor" href="#object-defineproperty" aria-label="Permalink to &quot;Object.defineProperty()&quot;">​</a></h2><p>在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回这个对象 (拦截的是对象的属性，会改变原有的对象)</p><h2 id="语法" tabindex="-1">语法 <a class="header-anchor" href="#语法" aria-label="Permalink to &quot;语法&quot;">​</a></h2><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">Object.defineProperty(obj, prop, descriptor)</span></span></code></pre></div><h2 id="参数" tabindex="-1">参数 <a class="header-anchor" href="#参数" aria-label="Permalink to &quot;参数&quot;">​</a></h2><ul><li><p><code>obj</code>: 要添加对象的属性</p></li><li><p><code>prop</code>: 要定义或修改属性的名称</p></li><li><p><code>descriptor</code>: 要定义或修改属性描述符</p></li></ul><h2 id="使用方法" tabindex="-1">使用方法 <a class="header-anchor" href="#使用方法" aria-label="Permalink to &quot;使用方法&quot;">​</a></h2><ul><li><p>基本使用方法</p></li><li><p>监听对象上的多个属性</p></li><li><p>深度监听对象</p></li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">const person = {</span></span>
<span class="line"><span style="color:#A6ACCD;">    name: &#39;小白&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    age: 18,</span></span>
<span class="line"><span style="color:#A6ACCD;">    object: {</span></span>
<span class="line"><span style="color:#A6ACCD;">      value: 1</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// 实现一个响应式函数</span></span>
<span class="line"><span style="color:#A6ACCD;">function defineProperty(obj, key, val) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    // 如果对象中的属性也是一个对象时，递归进入对象，进行监听</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (typeof val === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        Observer(val)</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    Object.defineProperty(obj, key, {</span></span>
<span class="line"><span style="color:#A6ACCD;">        get() {</span></span>
<span class="line"><span style="color:#A6ACCD;">            console.log(\`读取: \${key}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">            return val</span></span>
<span class="line"><span style="color:#A6ACCD;">        },</span></span>
<span class="line"><span style="color:#A6ACCD;">        set(newVal) {</span></span>
<span class="line"><span style="color:#A6ACCD;">            // 如果我们对对象中的某个属性值，从字符串更改成对象，我们应该如何监听新添加对象的属性值呢?</span></span>
<span class="line"><span style="color:#A6ACCD;">            if (typeof newVal === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">                Observer(key)</span></span>
<span class="line"><span style="color:#A6ACCD;">            }</span></span>
<span class="line"><span style="color:#A6ACCD;">            console.log(\`修改\${key}。值为: \${newVal}\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">            val = newVal</span></span>
<span class="line"><span style="color:#A6ACCD;">        }</span></span>
<span class="line"><span style="color:#A6ACCD;">    })</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">// 实现一个遍历函数Observer</span></span>
<span class="line"><span style="color:#A6ACCD;">// 我们通常在实际情况中，需要监听对象中多个属性的变化</span></span>
<span class="line"><span style="color:#A6ACCD;">// 这个时候我们需要配合Object.keys()进行遍历</span></span>
<span class="line"><span style="color:#A6ACCD;">function Observer(obj) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    // 如果传入的值不是一个对象时候，return</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (typeof obj !== &#39;object&#39; || obj === null) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        return</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    Object.keys(obj).forEach((key) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">        defineProperty(obj, key, obj[key])</span></span>
<span class="line"><span style="color:#A6ACCD;">    })</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">Observer(person)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(person.name)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(person.name = &#39;小黑&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(person.object.value = &#39;2&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div><h2 id="总结" tabindex="-1">总结 <a class="header-anchor" href="#总结" aria-label="Permalink to &quot;总结&quot;">​</a></h2><p>通过以上对es5的<code>Object.defineProperty()</code>了解中，其实我们还有问题没有解决。 当我们要给对象新增属性时候，我们也需要手动去监听这个新增属性</p><blockquote><p><code>Object.defineProperty</code>无法监控到数组下标的变化，导致直接通过数组的下标给数组设置值，不能实时响应</p><p>为了解决这个问题，vue内部经过处理后可以使用几种方法来监听数组<code>push()</code>, <code>pop()</code>, <code>shift()</code>, <code>unshift()</code>, <code>splice()</code>, <code>sort()</code>, <code>reverse()</code></p><p>正是因为这个原因，在使用vue给<code>data</code>中的数组或者对象新增属性时候，需要使用vm.$set去更新视图，才能保证新增的属性属于响应式的</p><p>可以看到，通过<code>Object.definePorperty()</code>进行数据监听是比较麻烦的，需要大量的手动处理.继而在vue3中转而采用<code>Proxy</code></p></blockquote><h2 id="vue3响应式数据原理-proxy" tabindex="-1">Vue3响应式数据原理 --- Proxy <a class="header-anchor" href="#vue3响应式数据原理-proxy" aria-label="Permalink to &quot;Vue3响应式数据原理 --- Proxy&quot;">​</a></h2><h2 id="介绍" tabindex="-1">介绍 <a class="header-anchor" href="#介绍" aria-label="Permalink to &quot;介绍&quot;">​</a></h2><p>Proxy 对象用于创建一个对象的代理，从而实现基本操作的拦截和自定义（如属性查找，赋值，枚举，函数调用等）</p><h2 id="语法-1" tabindex="-1">语法 <a class="header-anchor" href="#语法-1" aria-label="Permalink to &quot;语法&quot;">​</a></h2><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">const p = new Proxy(target, handler)</span></span></code></pre></div><h2 id="参数-1" tabindex="-1">参数 <a class="header-anchor" href="#参数-1" aria-label="Permalink to &quot;参数&quot;">​</a></h2><p><code>target</code></p><p>要使用<code>Proxy</code>包装的目标对象（可以是任何类型的对象，包括原生数组，函数，甚至另一个代理）</p><p><code>handler</code></p><p>一个通常以函数作为属性的对象，各属性中的函数分别定义了在执行各种操作时代理<code>p</code>的行为</p><h2 id="使用方法-1" tabindex="-1">使用方法 <a class="header-anchor" href="#使用方法-1" aria-label="Permalink to &quot;使用方法&quot;">​</a></h2><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">const person = {</span></span>
<span class="line"><span style="color:#A6ACCD;">    name: &#39;小白&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    age: 18</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">const proxy = new Proxy(person, {</span></span>
<span class="line"><span style="color:#A6ACCD;">    // target代表源数据</span></span>
<span class="line"><span style="color:#A6ACCD;">   // propName代表操作的那个属性名</span></span>
<span class="line"><span style="color:#A6ACCD;">    get(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        console.log(\`读取proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">        return target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">    },</span></span>
<span class="line"><span style="color:#A6ACCD;">    set(target, propName, value) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        console.log(\`修改proxy: \${propName}属性。值为: \${value}\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">        return target[propName] = value</span></span>
<span class="line"><span style="color:#A6ACCD;">    },</span></span>
<span class="line"><span style="color:#A6ACCD;">    deleteProperty(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        console.log(\`删除proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">        return delete target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(proxy.name)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(proxy.name = &#39;小黑&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(delete proxy.name, proxy)</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div><p>可以看出，<code>Proxy</code>代理的是整个对象，而不是对象的某个特定属性，不需要我们通过遍历来逐个进行数据绑定</p><blockquote><p>值得注意的是：我们在使用<code>Object.definePorperty()</code>对对象添加属性的时候，我们对对象的读写操作针对还是对象本身</p><p>在<code>Proxy</code>中，如果需要进行读写操作，那么我们需要对实例对象<code>proxy</code>进行操作</p></blockquote><h2 id="解决object-defineporperty-中遇到的问题" tabindex="-1">解决Object.definePorperty()中遇到的问题 <a class="header-anchor" href="#解决object-defineporperty-中遇到的问题" aria-label="Permalink to &quot;解决Object.definePorperty()中遇到的问题&quot;">​</a></h2><p>在上面使用<code>Object.definePorperty()</code>的时候，我们遇到的问题有：</p><ul><li><p>一次只能对一个属性进行监听，需要遍历来对所有属性进行监听。（我们上面已经解决了）</p></li><li><p>在遇到一个对象的属性还是一个对象的情况下，需要递归监听</p></li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">const person = {</span></span>
<span class="line"><span style="color:#A6ACCD;">    name: &#39;小白&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    age: 18,</span></span>
<span class="line"><span style="color:#A6ACCD;">    object: {</span></span>
<span class="line"><span style="color:#A6ACCD;">        value: 1</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">const proxy = new Proxy(person, {</span></span>
<span class="line"><span style="color:#A6ACCD;">    // target代表源数据</span></span>
<span class="line"><span style="color:#A6ACCD;">   // propName代表操作的那个属性名</span></span>
<span class="line"><span style="color:#A6ACCD;">    get(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        console.log(\`读取proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">        return target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">    },</span></span>
<span class="line"><span style="color:#A6ACCD;">    set(target, propName, value) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        console.log(\`修改proxy: \${propName}属性。值为: \${value}\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">        return target[propName] = value</span></span>
<span class="line"><span style="color:#A6ACCD;">    },</span></span>
<span class="line"><span style="color:#A6ACCD;">    deleteProperty(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        console.log(\`删除proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">        return delete target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// console.log(proxy.name)</span></span>
<span class="line"><span style="color:#A6ACCD;">// console.log(proxy.name = &#39;小黑&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(proxy.object.value = &#39;2&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">// console.log(delete proxy.name, proxy)</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div><p>可以看到，当我们访问proxy的深层属性时候，并不会触发set。 所有proxy如果想要实现深度监听，也需要实现上文Observer的递归函数，使用proxy逐个对对象中的每个属性进行拦截</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">const person = {</span></span>
<span class="line"><span style="color:#A6ACCD;">    name: &#39;小白&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    age: 18,</span></span>
<span class="line"><span style="color:#A6ACCD;">    object: {</span></span>
<span class="line"><span style="color:#A6ACCD;">        value: 1</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">function deepProxy(obj, cb) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (typeof obj === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        ObserverProxy(obj)</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    return new Proxy(obj, {</span></span>
<span class="line"><span style="color:#A6ACCD;">        get(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">       console.log(\`读取proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">       return target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">        },</span></span>
<span class="line"><span style="color:#A6ACCD;">        set(target, propName, value) {</span></span>
<span class="line"><span style="color:#A6ACCD;">            if (typeof propName === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">                value = deepProxy(value, cb)</span></span>
<span class="line"><span style="color:#A6ACCD;">            }</span></span>
<span class="line"><span style="color:#A6ACCD;">       console.log(\`修改proxy: \${propName}属性。值为: \${value}\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">       return target[propName] = value</span></span>
<span class="line"><span style="color:#A6ACCD;">        },</span></span>
<span class="line"><span style="color:#A6ACCD;">        deleteProperty(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">       console.log(\`删除proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">       return delete target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">        }</span></span>
<span class="line"><span style="color:#A6ACCD;">    })</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">function ObserverProxy(obj, cb) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    for (let key in obj) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        if (typeof obj[key] === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">            obj[key] = deepProxy(obj[key], cb)</span></span>
<span class="line"><span style="color:#A6ACCD;">        }</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">let b = deepProxy(person, (type, data) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    console.log(type, data)</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(b)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(b.name)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(b.object.value = &#39;2&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div><ul><li>对于对象的新增属性，需要手动监听</li></ul><p>访问的proxy.value就是原本对象上不存在的属性，但是我们访问它的时候，仍然们可以被get拦截到。</p><ul><li>对于数组通过push、unshift方法增加的元素，也无法监听</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">const person = {</span></span>
<span class="line"><span style="color:#A6ACCD;">    name: &#39;小白&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    age: 18,</span></span>
<span class="line"><span style="color:#A6ACCD;">    object: {</span></span>
<span class="line"><span style="color:#A6ACCD;">        value: 1</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">function deepProxy(obj, cb) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (typeof obj === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        ObserverProxy(obj)</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    return new Proxy(obj, {</span></span>
<span class="line"><span style="color:#A6ACCD;">        get(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">       console.log(\`读取proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">       return target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">        },</span></span>
<span class="line"><span style="color:#A6ACCD;">        set(target, propName, value, receiver) {</span></span>
<span class="line"><span style="color:#A6ACCD;">            if (typeof propName === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">                value = deepProxy(value, cb)</span></span>
<span class="line"><span style="color:#A6ACCD;">            }</span></span>
<span class="line"><span style="color:#A6ACCD;">            let cbType = target[propName] === undefined ? &#39;create&#39; : &#39;modify&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">            // 排除数组修改length回调</span></span>
<span class="line"><span style="color:#A6ACCD;">            if (!(Array.isArray(target) &amp;&amp; propName === &#39;length&#39;)) {</span></span>
<span class="line"><span style="color:#A6ACCD;">                cb(cbType, {target, propName, value})</span></span>
<span class="line"><span style="color:#A6ACCD;">            }</span></span>
<span class="line"><span style="color:#A6ACCD;">       console.log(\`修改proxy: \${propName}属性。值为: \${value}\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">       return Reflect.set(target, propName, value, receiver)</span></span>
<span class="line"><span style="color:#A6ACCD;">        },</span></span>
<span class="line"><span style="color:#A6ACCD;">        deleteProperty(target, propName) {</span></span>
<span class="line"><span style="color:#A6ACCD;">       console.log(\`删除proxy: \${propName}属性\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">       return delete target[propName]</span></span>
<span class="line"><span style="color:#A6ACCD;">        }</span></span>
<span class="line"><span style="color:#A6ACCD;">    })</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">function ObserverProxy(obj, cb) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    for (let key in obj) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        if (typeof obj[key] === &#39;object&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">            obj[key] = deepProxy(obj[key], cb)</span></span>
<span class="line"><span style="color:#A6ACCD;">        }</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">let a = deepProxy([&#39;小白&#39;], (type, data) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    console.log(type, data)</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(a)</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(a.push(&#39;小黑&#39;))</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div>`,37),l=[p],C=JSON.parse('{"title":"proxy -- Object.defineProperty() Vue双向绑定的原理","description":"","frontmatter":{},"headers":[],"relativePath":"vue3/proxy.md","filePath":"vue3/proxy.md"}'),e={name:"vue3/proxy.md"},A=Object.assign(e,{setup(o){return(c,t)=>(s(),n("div",null,l))}});export{C as __pageData,A as default};
