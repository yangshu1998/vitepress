import{o as s,c as a,O as n}from"./chunks/framework.c9b07a32.js";const l=n(`<h1 id="hook函数-组合式函数" tabindex="-1">hook函数（组合式函数） <a class="header-anchor" href="#hook函数-组合式函数" aria-label="Permalink to &quot;hook函数（组合式函数）&quot;">​</a></h1><h2 id="介绍" tabindex="-1">介绍 <a class="header-anchor" href="#介绍" aria-label="Permalink to &quot;介绍&quot;">​</a></h2><ul><li><p>在vue应用中，<code>组合式函数</code>是一个利用vue的组合式api来封装和复用<code>有状态逻辑</code>的函数</p></li><li><p>vue3的hook函数，相当于vue2的mixin，不同于在于hooks是函数</p></li><li><p>vue3的hook函数，可以帮助我们提高代码的复用性，让我们在不同的组件中都利用hooks函数</p></li></ul><h2 id="分页获取列表实例" tabindex="-1">分页获取列表实例 <a class="header-anchor" href="#分页获取列表实例" aria-label="Permalink to &quot;分页获取列表实例&quot;">​</a></h2><p>我们直接在组件中使用组合式api实现获取分页列表功能,</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { ref, getCurrentInstance, onMounted } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">const { proxy } = getCurrentInstance()</span></span>
<span class="line"><span style="color:#A6ACCD;">const list = ref([])</span></span>
<span class="line"><span style="color:#A6ACCD;">const getMusicList = async () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    const { data: res } = await proxy.$http.getMusicList()</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (res.code == 200) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        list.value = res.playlists</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">onMounted(() =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    getMusicList()</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div><p>如果我们有多个list页面（接口返回格式要相同），我们要如何去复用这种相同的逻辑呢？我们可以把这个逻辑以一个组合式函数的形式提到外部文件：</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;!-- useList.js --&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { ref } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">const useList = (listReq, data) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (!listReq) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        return new Error(&#39;请传入接口调用方法&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    // 判断第二个参数是否是对象，以免后面使用展开运算符时报错</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (data &amp;&amp; Object.prototype.toString.call(data) !== &#39;[object Object]&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        return new Error(&#39;额外参数请使用对象传入&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    const list = ref([])</span></span>
<span class="line"><span style="color:#A6ACCD;">    // 新增pageInfo对象保存分页数据</span></span>
<span class="line"><span style="color:#A6ACCD;">    const pageInfo = ref({</span></span>
<span class="line"><span style="color:#A6ACCD;">        pageNum: 1,</span></span>
<span class="line"><span style="color:#A6ACCD;">        pageSize: 10,</span></span>
<span class="line"><span style="color:#A6ACCD;">        total: 0</span></span>
<span class="line"><span style="color:#A6ACCD;">    })</span></span>
<span class="line"><span style="color:#A6ACCD;">    const getList = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">        const params = {</span></span>
<span class="line"><span style="color:#A6ACCD;">            ...pageInfo.value,</span></span>
<span class="line"><span style="color:#A6ACCD;">            ...data</span></span>
<span class="line"><span style="color:#A6ACCD;">        }</span></span>
<span class="line"><span style="color:#A6ACCD;">        listReq(params).then((res) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">            if (params.pageNum === 1) {</span></span>
<span class="line"><span style="color:#A6ACCD;">                list.value = res.data.playlists</span></span>
<span class="line"><span style="color:#A6ACCD;">            } else {</span></span>
<span class="line"><span style="color:#A6ACCD;">                list.value = [...list.value, ...res.data.playlists]</span></span>
<span class="line"><span style="color:#A6ACCD;">            }</span></span>
<span class="line"><span style="color:#A6ACCD;">            // 更新总数量</span></span>
<span class="line"><span style="color:#A6ACCD;">            params.total = res.data.total</span></span>
<span class="line"><span style="color:#A6ACCD;">        })</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    // 新增加载下一页的函数</span></span>
<span class="line"><span style="color:#A6ACCD;">    const loadMore = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">        pageInfo.value.pageNum += 1</span></span>
<span class="line"><span style="color:#A6ACCD;">        getList()</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    // 新增初始化</span></span>
<span class="line"><span style="color:#A6ACCD;">    const initList = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">        // 初始化一般重置所有初始条件，</span></span>
<span class="line"><span style="color:#A6ACCD;">        pageInfo.value.pageNum = 1</span></span>
<span class="line"><span style="color:#A6ACCD;">        getList()</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">    getList()</span></span>
<span class="line"><span style="color:#A6ACCD;">    return {</span></span>
<span class="line"><span style="color:#A6ACCD;">        list,</span></span>
<span class="line"><span style="color:#A6ACCD;">        getList,</span></span>
<span class="line"><span style="color:#A6ACCD;">        loadMore,</span></span>
<span class="line"><span style="color:#A6ACCD;">        initList</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">// const { list: goodsList, getList: getGoodsList } = useList(</span></span>
<span class="line"><span style="color:#A6ACCD;">//   axios.get(&#39;/url/get/goods&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">// )</span></span>
<span class="line"><span style="color:#A6ACCD;">// const { list: recommendList, getList: getRecommendList } = useList(</span></span>
<span class="line"><span style="color:#A6ACCD;">//   axios.get(&#39;/url/get/goods&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">// )</span></span>
<span class="line"><span style="color:#A6ACCD;">export default useList</span></span></code></pre></div><p>下面是他组件中的使用方式</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { getCurrentInstance } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import useList from &#39;@/hooks/useList.js&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">const { proxy } = getCurrentInstance()</span></span>
<span class="line"><span style="color:#A6ACCD;">const { list } = useList(proxy.$http.getMusicList, {})</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div><p>综上所述：核心逻辑完全一致，我们做的只是把它移动到另外一个外部函数中去，并返回需要暴露的状态，和在组件中一样，你也可以在组合式函数中使用所有的<code>组合式api</code></p><h2 id="约定和最佳实践" tabindex="-1">约定和最佳实践 <a class="header-anchor" href="#约定和最佳实践" aria-label="Permalink to &quot;约定和最佳实践&quot;">​</a></h2><h3 id="命名" tabindex="-1">命名 <a class="header-anchor" href="#命名" aria-label="Permalink to &quot;命名&quot;">​</a></h3><p>组合式函数约定驼峰命名法命名，并以<code>use</code>作为开头</p><h3 id="使用限制" tabindex="-1">使用限制 <a class="header-anchor" href="#使用限制" aria-label="Permalink to &quot;使用限制&quot;">​</a></h3><p>组合式函数在<code>&lt;script setup&gt;&lt;/script&gt;</code>或<code>setup()</code>钩子中，应始终被<code>同步</code>调用，在某些场景下，你也可以在像<code>onMounted()</code>这样的生命周期钩子中使用他们</p><p>这个限制是为了让 Vue 能够确定当前正在被执行的到底是哪个组件实例，只有能确认当前组件实例，才能够：</p><ul><li><p>将生命周期钩子注册到该组件实例上</p></li><li><p>将计算属性和监听器注册到该组件实例上，以便在该组件被卸载时停止监听，避免内存泄漏。</p></li></ul><blockquote><p>TIP</p><p><code>&lt;script setup&gt;</code> 是唯一在调用 <code>await </code>之后仍可调用组合式函数的地方。编译器会在异步操作之后自动为你恢复当前的组件实例。</p></blockquote><h2 id="与其他模式的比较" tabindex="-1">与其他模式的比较 <a class="header-anchor" href="#与其他模式的比较" aria-label="Permalink to &quot;与其他模式的比较&quot;">​</a></h2><h3 id="和mixin的对比" tabindex="-1">和Mixin的对比 <a class="header-anchor" href="#和mixin的对比" aria-label="Permalink to &quot;和Mixin的对比&quot;">​</a></h3><p>Vue 2 的用户可能会对 <code>mixins</code> 选项比较熟悉。它也让我们能够把组件逻辑提取到可复用的单元里。然而 <code>mixins</code> 有三个主要的短板：</p><ul><li><p><code>不清晰的数据来源</code>: 当使用了多个 mixin 时，实例上的数据属性来自哪个 mixin 变得不清晰，这使追溯实现和理解组件行为变得困难。这也是我们推荐在组合式函数中使用 ref + 解构模式的理由：让属性的来源在消费组件时一目了然</p></li><li><p><code>命名空间冲突</code>: 多个来自不同作者的 mixin 可能会注册相同的属性名，造成命名冲突。若使用组合式函数，你可以通过在解构变量时对变量进行重命名来避免相同的键名。</p></li><li><p><code>隐式的跨 mixin 交流</code>: 多个 mixin 需要依赖共享的属性名来进行相互作用，这使得它们隐性地耦合在一起。而一个组合式函数的返回值可以作为另一个组合式函数的参数被传入，像普通函数那样。</p></li></ul>`,23),p=[l],r=JSON.parse('{"title":"hook函数（组合式函数）","description":"","frontmatter":{},"headers":[],"relativePath":"vue3/hook.md","filePath":"vue3/hook.md"}'),e={name:"vue3/hook.md"},C=Object.assign(e,{setup(o){return(t,c)=>(s(),a("div",null,p))}});export{r as __pageData,C as default};
