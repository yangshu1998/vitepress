import{o as s,c as n,O as a}from"./chunks/framework.c9b07a32.js";const p="/vitepress/assets/1.f39e5509.jpg",l="/vitepress/assets/2.4c64644b.jpg",e="/vitepress/assets/3.eda72eaa.jpg",o="/vitepress/assets/4.57fc3b7f.jpg",t=a('<h1 id="vite-vue3-vant-前端工程化项目环境" tabindex="-1">Vite + Vue3 + vant 前端工程化项目环境 <a class="header-anchor" href="#vite-vue3-vant-前端工程化项目环境" aria-label="Permalink to &quot;Vite + Vue3 + vant 前端工程化项目环境&quot;">​</a></h1><h2 id="技术栈" tabindex="-1">技术栈 <a class="header-anchor" href="#技术栈" aria-label="Permalink to &quot;技术栈&quot;">​</a></h2><p>编程语言： HTML CSS3 JavaScript<br> 构建工具：Vite 2.x<br> 前端框架：Vue 3.x<br> 路由工具：Vue Router 4.x<br> 状态关联：Vuex 4.x<br> UI框架：Vant<br> Css预编译：sass<br> HTTP工具：Axios<br></p><h2 id="架构搭建" tabindex="-1">架构搭建 <a class="header-anchor" href="#架构搭建" aria-label="Permalink to &quot;架构搭建&quot;">​</a></h2><p>本项目使用Vite构建工具，Vite 需要 Node.js 版本 14.18+，16+。然而，有些模板需要依赖更高的 Node 版本才能正常运行，当你的包管理器发出警告时，请注意升级你的 Node 版本。 本项目中的Node版本为 &gt;= 12.0.0</p><h2 id="使用-vite-快速初始化项目雏形" tabindex="-1">使用 Vite 快速初始化项目雏形 <a class="header-anchor" href="#使用-vite-快速初始化项目雏形" aria-label="Permalink to &quot;使用 Vite 快速初始化项目雏形&quot;">​</a></h2><ul><li>使用npm <blockquote><p><s>npm init @vitejs/app</s> 已经废弃</p><p>npm create vite@latest</p><p>npm create vite@latest my-vue-app -- --template vue</p></blockquote></li></ul><p>然后按照终端提示完成以下操作</p><p>1.输入项目名称, 选择模板</p><p><img src="'+p+'" alt="图片1" data-fancybox="gallery"></p><p>2.安装依赖，启动项目</p><blockquote><p>注意：</p><p>下图中因为当前node版本过低，所以会出现依赖警告（可以安装一个nvm）去切换node版本 <a href="https://www.jianshu.com/p/13c0b3ca7c71" target="_blank" rel="noreferrer">https://www.jianshu.com/p/13c0b3ca7c71 nvm安装教程</a></p></blockquote><p><img src="'+l+'" alt="图片1" data-fancybox="gallery"></p><p><img src="'+e+'" alt="图片1" data-fancybox="gallery"></p><p><img src="'+o+`" alt="图片1" data-fancybox="gallery"></p><p>如上图，表示 Vite + Vue3 简单的项目骨架搭建完毕，下面我们来为这个项目集成 Vue Router、Vuex、Vant、Axios、Sass。</p><h2 id="规范目录结构" tabindex="-1">规范目录结构 <a class="header-anchor" href="#规范目录结构" aria-label="Permalink to &quot;规范目录结构&quot;">​</a></h2><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">├─index.html</span></span>
<span class="line"><span style="color:#A6ACCD;">├─package-lock.json</span></span>
<span class="line"><span style="color:#A6ACCD;">├─package.json</span></span>
<span class="line"><span style="color:#A6ACCD;">├─README.md</span></span>
<span class="line"><span style="color:#A6ACCD;">├─vite.config.js   // Vite 配置文件</span></span>
<span class="line"><span style="color:#A6ACCD;">├─src</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─App.vue</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─main.js</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─style.css</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─views  //页面组件目录</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─utils  //工具函数目录</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─store  //状态管理目录</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─router //路由配置目录</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─components   //公共组件目录</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─base         //通用类库目录</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─assets       //静态资源目录</span></span>
<span class="line"><span style="color:#A6ACCD;">|  ├─api          //接口资源目录</span></span>
<span class="line"><span style="color:#A6ACCD;">├─public</span></span>
<span class="line"><span style="color:#A6ACCD;">├─.vscode</span></span></code></pre></div><h2 id="修改-vite-配置文件" tabindex="-1">修改 Vite 配置文件 <a class="header-anchor" href="#修改-vite-配置文件" aria-label="Permalink to &quot;修改 Vite 配置文件&quot;">​</a></h2><p>本内容讲解配置先做简单配置：</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { defineConfig } from &#39;vite&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import vue from &#39;@vitejs/plugin-vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { resolve } from &#39;path&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -&gt; npm i @types/node -D</span></span>
<span class="line"><span style="color:#A6ACCD;">// https://vitejs.dev/config/</span></span>
<span class="line"><span style="color:#A6ACCD;">export default defineConfig({</span></span>
<span class="line"><span style="color:#A6ACCD;">  plugins: [vue()],</span></span>
<span class="line"><span style="color:#A6ACCD;"> resolve: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  alias: {</span></span>
<span class="line"><span style="color:#A6ACCD;">   &#39;@&#39;: resolve(__dirname, &#39;src&#39;) // 设置 \`@\` 指向 \`src\` 目录</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> },</span></span>
<span class="line"><span style="color:#A6ACCD;"> base: &#39;/&#39;, //设置打包路径</span></span>
<span class="line"><span style="color:#A6ACCD;"> server: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  open: true, //启动自动打开浏览器</span></span>
<span class="line"><span style="color:#A6ACCD;">  hmr: true, //开启热更新</span></span>
<span class="line"><span style="color:#A6ACCD;">  port: &#39;80&#39;, //指定开发服务器端口</span></span>
<span class="line"><span style="color:#A6ACCD;">  host: &#39;0.0.0.0&#39;, //指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址</span></span>
<span class="line"><span style="color:#A6ACCD;">  cors: true, //为开发服务器配置 CORS</span></span>
<span class="line"><span style="color:#A6ACCD;">  proxy: {  //// 设置代理，根据我们项目实际情况配置</span></span>
<span class="line"><span style="color:#A6ACCD;">   &quot;/api&quot;: {</span></span>
<span class="line"><span style="color:#A6ACCD;">    target: &#39;&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    ws: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">    changeOrigin: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">    rewrite: (path) =&gt; path.replace(/^\\/api/, &#39;&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">   }</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span></code></pre></div><h2 id="配置路由工具-vue-router" tabindex="-1">配置路由工具 Vue Router <a class="header-anchor" href="#配置路由工具-vue-router" aria-label="Permalink to &quot;配置路由工具 Vue Router&quot;">​</a></h2><p>1.安装支持 Vue3 的路由工具 vue-router@4</p><blockquote><p>npm i vue-router@4</p></blockquote><p>2.创建相关文件</p><ul><li>src/router/index.js 文件</li><li>components/recommend/recommend.vue 文件</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { createRouter, createWebHashHistory } from &quot;vue-router&quot;;</span></span>
<span class="line"><span style="color:#A6ACCD;">import Recommend from &#39;@/components/recommend/recommend.vue&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">// 懒加载方式</span></span>
<span class="line"><span style="color:#A6ACCD;">// const Recommend = () =&gt; import(&#39;@/components/recommend/recommend.vue&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">const routes = [</span></span>
<span class="line"><span style="color:#A6ACCD;"> {</span></span>
<span class="line"><span style="color:#A6ACCD;">  path: &#39;/&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">  name: &#39;Recommend&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">  component: Recommend,</span></span>
<span class="line"><span style="color:#A6ACCD;"> },</span></span>
<span class="line"><span style="color:#A6ACCD;">]</span></span>
<span class="line"><span style="color:#A6ACCD;">const router = createRouter({</span></span>
<span class="line"><span style="color:#A6ACCD;">  history: createWebHashHistory(),</span></span>
<span class="line"><span style="color:#A6ACCD;">  routes</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;">export default router</span></span></code></pre></div><p>3.在main.js文件中挂载路由, App.vue 编写 <code>&lt;router-view&gt;&lt;/router-view&gt;</code></p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { createApp } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import &#39;./style.css&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import App from &#39;./App.vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import router from &#39;./router&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// createApp(App).mount(&#39;#app&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">const app = createApp(App)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(router)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.mount(&#39;#app&#39;)</span></span></code></pre></div><h2 id="配置状态管理工具-vuex" tabindex="-1">配置状态管理工具 Vuex <a class="header-anchor" href="#配置状态管理工具-vuex" aria-label="Permalink to &quot;配置状态管理工具 Vuex&quot;">​</a></h2><p>1.安装支持 Vue3 的状态管理工具 vuex@next</p><blockquote><p>npm i vuex@next</p></blockquote><p>2.创建相关文件</p><ul><li>src/store/index.js 文件</li><li>src/store/modules/storeState.js 文件</li><li>src/store/actions.js</li><li>src/store/getter.js</li></ul><p>3.在 main.js 文件中挂载 Vuex 配置</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { createApp } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import &#39;./style.css&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import App from &#39;./App.vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import router from &#39;./router&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import store from &#39;./store&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// createApp(App).mount(&#39;#app&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">const app = createApp(App)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(router)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(store)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.mount(&#39;#app&#39;)</span></span></code></pre></div><h2 id="集成-ui-框架-vant" tabindex="-1">集成 UI 框架 Vant <a class="header-anchor" href="#集成-ui-框架-vant" aria-label="Permalink to &quot;集成 UI 框架 Vant&quot;">​</a></h2><p>1.在 vite 项目中按需引入组件</p><p>2.在 vite 项目中使用 Vant 时，推荐安装 <code>vite-plugin-style-import</code> 插件，它可以自动按需引入组件的样式。</p><blockquote><p>npm i vant</p><p>npm i vite-plugin-style-import@1.4.1 -D</p></blockquote><p>3.安装完成后，在 vite.config.js 文件中配置插件：</p><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { defineConfig } from &#39;vite&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import vue from &#39;@vitejs/plugin-vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { resolve } from &#39;path&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -&gt; npm i @types/node -D</span></span>
<span class="line"><span style="color:#A6ACCD;">import styleImport, { VantResolve } from &#39;vite-plugin-style-import&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">// https://vitejs.dev/config/</span></span>
<span class="line"><span style="color:#A6ACCD;">export default defineConfig({</span></span>
<span class="line"><span style="color:#A6ACCD;">  plugins: [</span></span>
<span class="line"><span style="color:#A6ACCD;">  vue(),</span></span>
<span class="line"><span style="color:#A6ACCD;">  styleImport({</span></span>
<span class="line"><span style="color:#A6ACCD;">   resolves: [VantResolve()],</span></span>
<span class="line"><span style="color:#A6ACCD;">   // 按照官网配置会报错，此处需添加libs代码，引入的样式多了lib</span></span>
<span class="line"><span style="color:#A6ACCD;">   libs: [</span></span>
<span class="line"><span style="color:#A6ACCD;">    {</span></span>
<span class="line"><span style="color:#A6ACCD;">     libraryName: &#39;vant&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">     esModule: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">     resolveStyle: name =&gt; \`../es/\${name}/style\`</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">   ]</span></span>
<span class="line"><span style="color:#A6ACCD;">  })</span></span>
<span class="line"><span style="color:#A6ACCD;"> ],</span></span>
<span class="line"><span style="color:#A6ACCD;"> resolve: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  alias: {</span></span>
<span class="line"><span style="color:#A6ACCD;">   &#39;@&#39;: resolve(__dirname, &#39;src&#39;) // 设置 \`@\` 指向 \`src\` 目录</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> },</span></span>
<span class="line"><span style="color:#A6ACCD;"> base: &#39;/&#39;, //设置打包路径</span></span>
<span class="line"><span style="color:#A6ACCD;"> server: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  open: true, //启动自动打开浏览器</span></span>
<span class="line"><span style="color:#A6ACCD;">  hmr: true, //开启热更新</span></span>
<span class="line"><span style="color:#A6ACCD;">  port: &#39;80&#39;, //指定开发服务器端口</span></span>
<span class="line"><span style="color:#A6ACCD;">  host: &#39;0.0.0.0&#39;, //指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址</span></span>
<span class="line"><span style="color:#A6ACCD;">  cors: true, //为开发服务器配置 CORS</span></span>
<span class="line"><span style="color:#A6ACCD;">  proxy: {  //// 设置代理，根据我们项目实际情况配置</span></span>
<span class="line"><span style="color:#A6ACCD;">   &quot;/api&quot;: {</span></span>
<span class="line"><span style="color:#A6ACCD;">    target: &#39;&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    ws: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">    changeOrigin: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">    rewrite: (path) =&gt; path.replace(/^\\/api/, &#39;&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">   }</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span></code></pre></div><p>4.配置基于 Viewport 的适配方案</p><ol><li>安装<code>postcss-px-to-viewport-8-plugin</code></li></ol><blockquote><p>npm i postcss-px-to-viewport-8-plugin</p></blockquote><ol start="2"><li>安装完成后，在 vite.config.js 文件中配置插件：</li></ol><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { defineConfig } from &#39;vite&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import vue from &#39;@vitejs/plugin-vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { resolve } from &#39;path&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -&gt; npm i @types/node -D</span></span>
<span class="line"><span style="color:#A6ACCD;">import styleImport, { VantResolve } from &#39;vite-plugin-style-import&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">import postcsspxtoviewport8plugin from &#39;postcss-px-to-viewport-8-plugin&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// https://vitejs.dev/config/</span></span>
<span class="line"><span style="color:#A6ACCD;">export default defineConfig({</span></span>
<span class="line"><span style="color:#A6ACCD;">  plugins: [</span></span>
<span class="line"><span style="color:#A6ACCD;">  vue(),</span></span>
<span class="line"><span style="color:#A6ACCD;">  styleImport({</span></span>
<span class="line"><span style="color:#A6ACCD;">   resolves: [VantResolve()],</span></span>
<span class="line"><span style="color:#A6ACCD;">   // 按照官网配置会报错，此处需添加libs代码，引入的样式多了lib</span></span>
<span class="line"><span style="color:#A6ACCD;">   libs: [</span></span>
<span class="line"><span style="color:#A6ACCD;">    {</span></span>
<span class="line"><span style="color:#A6ACCD;">     libraryName: &#39;vant&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">     esModule: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">     resolveStyle: name =&gt; \`../es/\${name}/style\`</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">   ]</span></span>
<span class="line"><span style="color:#A6ACCD;">  })</span></span>
<span class="line"><span style="color:#A6ACCD;"> ],</span></span>
<span class="line"><span style="color:#A6ACCD;"> resolve: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  alias: {</span></span>
<span class="line"><span style="color:#A6ACCD;">   &#39;@&#39;: resolve(__dirname, &#39;src&#39;) // 设置 \`@\` 指向 \`src\` 目录</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> },</span></span>
<span class="line"><span style="color:#A6ACCD;"> base: &#39;/&#39;, //设置打包路径</span></span>
<span class="line"><span style="color:#A6ACCD;"> server: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  open: true, //启动自动打开浏览器</span></span>
<span class="line"><span style="color:#A6ACCD;">  hmr: true, //开启热更新</span></span>
<span class="line"><span style="color:#A6ACCD;">  port: &#39;80&#39;, //指定开发服务器端口</span></span>
<span class="line"><span style="color:#A6ACCD;">  host: &#39;0.0.0.0&#39;, //指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址</span></span>
<span class="line"><span style="color:#A6ACCD;">  cors: true, //为开发服务器配置 CORS</span></span>
<span class="line"><span style="color:#A6ACCD;">  proxy: {  //// 设置代理，根据我们项目实际情况配置</span></span>
<span class="line"><span style="color:#A6ACCD;">   &quot;/api&quot;: {</span></span>
<span class="line"><span style="color:#A6ACCD;">    target: &#39;&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    ws: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">    changeOrigin: true,</span></span>
<span class="line"><span style="color:#A6ACCD;">    rewrite: (path) =&gt; path.replace(/^\\/api/, &#39;&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">   }</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> },</span></span>
<span class="line"><span style="color:#A6ACCD;"> css: {</span></span>
<span class="line"><span style="color:#A6ACCD;">  postcss: {</span></span>
<span class="line"><span style="color:#A6ACCD;">   plugins: [</span></span>
<span class="line"><span style="color:#A6ACCD;">    postcsspxtoviewport8plugin({</span></span>
<span class="line"><span style="color:#A6ACCD;">     unitToConvert: &#39;px&#39;, // 要转化的单位</span></span>
<span class="line"><span style="color:#A6ACCD;">     viewportWidth: 375, // UI设计稿的宽度</span></span>
<span class="line"><span style="color:#A6ACCD;">     unitPrecision: 6, // 转换后的精度，即小数点位数</span></span>
<span class="line"><span style="color:#A6ACCD;">     propList: [&#39;*&#39;], // 指定转换的css属性的单位，*代表全部css属性的单位都进行转换</span></span>
<span class="line"><span style="color:#A6ACCD;">     viewportUnit: &#39;vw&#39;, // 指定需要转换成的视窗单位，默认vw</span></span>
<span class="line"><span style="color:#A6ACCD;">     fontViewportUnit: &#39;vw&#39;, // 指定字体需要转换成的视窗单位，默认vw</span></span>
<span class="line"><span style="color:#A6ACCD;">     selectorBlackList: [&#39;ignore-&#39;], // 指定不转换为视窗单位的类名，</span></span>
<span class="line"><span style="color:#A6ACCD;">     minPixelValue: 1, // 默认值1，小于或等于1px则不进行转换</span></span>
<span class="line"><span style="color:#A6ACCD;">     mediaQuery: true, // 是否在媒体查询的css代码中也进行转换，默认false</span></span>
<span class="line"><span style="color:#A6ACCD;">     replace: true, // 是否转换后直接更换属性值</span></span>
<span class="line"><span style="color:#A6ACCD;">     // exclude: [/node_modules/], // 设置忽略文件，用正则做目录名匹配</span></span>
<span class="line"><span style="color:#A6ACCD;">     exclude: [],</span></span>
<span class="line"><span style="color:#A6ACCD;">     landscape: false // 是否处理横屏情况</span></span>
<span class="line"><span style="color:#A6ACCD;">    })</span></span>
<span class="line"><span style="color:#A6ACCD;">   ]</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span></code></pre></div><ol start="3"><li>配置完成后,可在main.js 直接使用组件或者在单页面中调用组件</li></ol><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { createApp } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import &#39;./style.css&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import App from &#39;./App.vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import router from &#39;./router&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import store from &#39;./store&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import { Button } from &#39;vant&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">// createApp(App).mount(&#39;#app&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">const app = createApp(App)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(router)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(store)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(Button)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.mount(&#39;#app&#39;)</span></span></code></pre></div><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;h1&gt;helloword&lt;/h1&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;Button type=&quot;primary&quot;&gt;主要按钮&lt;/Button&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;cell-group&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">   &lt;cell title=&quot;单元格&quot; value=&quot;内容&quot; /&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">   &lt;cell title=&quot;单元格&quot; value=&quot;内容&quot; label=&quot;描述信息&quot; /&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> &lt;/cell-group&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/template&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> import { Button, Cell, CellGroup  } from &#39;vant&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;style&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> h1 {</span></span>
<span class="line"><span style="color:#A6ACCD;">  width: 100px;</span></span>
<span class="line"><span style="color:#A6ACCD;"> }</span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/style&gt;</span></span></code></pre></div><h2 id="集成-http-工具-axios" tabindex="-1">集成 HTTP 工具 Axios <a class="header-anchor" href="#集成-http-工具-axios" aria-label="Permalink to &quot;集成 HTTP 工具 Axios&quot;">​</a></h2><ol><li>安装Axios</li></ol><blockquote><p>npm i axios</p></blockquote><ol start="2"><li>配置axios</li></ol><ul><li>在src/api/axios.js 新建配置文件</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import Axios from &#39;axios&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">let baseURL = &#39;&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">if (i<wbr>mport.meta.env.MODE  === &#39;development&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">  baseURL = &#39;/api&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">} else if (i<wbr>mport.meta.env.MODE === &#39;production&#39;) {</span></span>
<span class="line"><span style="color:#A6ACCD;">  baseURL = i<wbr>mport.meta.env.VITE_BASE_URL</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">console.log(baseURL)</span></span>
<span class="line"><span style="color:#A6ACCD;">/*  防止请求重复</span></span>
<span class="line"><span style="color:#A6ACCD;">1. 我们需要对所有正在进行中的请求进行缓存。在请求发起前判断缓存列表中该请求是否正在进行，如果有则取消本次请求。</span></span>
<span class="line"><span style="color:#A6ACCD;">2.在任意请求完成后，需要在缓存列表中删除该次请求，以便可以重新发送该请求</span></span>
<span class="line"><span style="color:#A6ACCD;">*/</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">//正在请求的API队列</span></span>
<span class="line"><span style="color:#A6ACCD;">let requestList = []</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">/**</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @name:  阻止请求</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @param {array} requestList 当前API请求队列</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @param {string} currentUrl  当前请求API</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @param {function} cancelFn  请求中断函数</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @param {string} errorMsg   中断错误信息</span></span>
<span class="line"><span style="color:#A6ACCD;"> */</span></span>
<span class="line"><span style="color:#A6ACCD;"> const stopRepeatRequest = (requestList, currentUrl, cancelFn, errorMsg) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  const errorMessage = errorMsg || &#39;请求出错拥堵&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">  for (let i = 0; i &lt; requestList.length; i++) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (requestList[i] === currentUrl) {</span></span>
<span class="line"><span style="color:#A6ACCD;">      cancelFn(errorMessage)</span></span>
<span class="line"><span style="color:#A6ACCD;">      return</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;">  // 将当前请求加入执行队列</span></span>
<span class="line"><span style="color:#A6ACCD;">  requestList.push(currentUrl)</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">/**</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @name:  请求完成后从队列删除当前请求</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @param {array} requestList 当前API请求队列</span></span>
<span class="line"><span style="color:#A6ACCD;"> * @param {string} currentUrl  当前请求API</span></span>
<span class="line"><span style="color:#A6ACCD;"> */</span></span>
<span class="line"><span style="color:#A6ACCD;"> const allowRequest = (requestList, currentUrl) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  for (let i = 0; i &lt; requestList.length; i++) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    if (requestList[i] === currentUrl) {</span></span>
<span class="line"><span style="color:#A6ACCD;">      requestList.splice(i, 1)</span></span>
<span class="line"><span style="color:#A6ACCD;">      break</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">const instance = Axios.create({</span></span>
<span class="line"><span style="color:#A6ACCD;">  baseURL,</span></span>
<span class="line"><span style="color:#A6ACCD;">  timeout: 2000,</span></span>
<span class="line"><span style="color:#A6ACCD;">  headers: {</span></span>
<span class="line"><span style="color:#A6ACCD;">    &#39;Content-Type&#39;: &#39;application/json&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">    &#39;X-Requested-With&#39;: &#39;XMLHttpRequest&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// 前置拦截器(发起请求前拦截)</span></span>
<span class="line"><span style="color:#A6ACCD;">instance.interceptors.request.use((config) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  // 这里根据你项目实际情况来对config做处理</span></span>
<span class="line"><span style="color:#A6ACCD;">  // 设置cancelToken</span></span>
<span class="line"><span style="color:#A6ACCD;">  let cancelFn = null;</span></span>
<span class="line"><span style="color:#A6ACCD;">  config.cancelToken = new Axios.CancelToken((c) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    cancelFn = c;</span></span>
<span class="line"><span style="color:#A6ACCD;">  })</span></span>
<span class="line"><span style="color:#A6ACCD;">  // 阻止重复请求</span></span>
<span class="line"><span style="color:#A6ACCD;">  stopRepeatRequest(requestList, config.url, cancelFn, \`不要连续请求：\${config.url}，速度太快了\`)</span></span>
<span class="line"><span style="color:#A6ACCD;">  if (localStorage.getItem(&#39;token&#39;)) {</span></span>
<span class="line"><span style="color:#A6ACCD;">    config.headers.common[&#39;Authorization&#39;] = localStorage.getItem(&#39;token&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">  }</span></span>
<span class="line"><span style="color:#A6ACCD;">  return config</span></span>
<span class="line"><span style="color:#A6ACCD;">}, (error) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  return Promise.reject(error)</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// 后置拦截器(获取到响应式的拦截)</span></span>
<span class="line"><span style="color:#A6ACCD;">instance.interceptors.response.use((response) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  //不得重复发送</span></span>
<span class="line"><span style="color:#A6ACCD;">  setTimeout(() =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    allowRequest(requestList, response.config.url), 1000</span></span>
<span class="line"><span style="color:#A6ACCD;">  })</span></span>
<span class="line"><span style="color:#A6ACCD;">  return response</span></span>
<span class="line"><span style="color:#A6ACCD;">}, (error) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  return Promise.reject(error)</span></span>
<span class="line"><span style="color:#A6ACCD;">})</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">// 对错误信息的处理函数</span></span>
<span class="line"><span style="color:#A6ACCD;">const ajaxMethod = [&#39;get&#39;, &#39;post&#39;]</span></span>
<span class="line"><span style="color:#A6ACCD;">const api = {}</span></span>
<span class="line"><span style="color:#A6ACCD;">ajaxMethod.forEach(method =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">    api[method] = function (uri, data, config) {</span></span>
<span class="line"><span style="color:#A6ACCD;">        return new Promise(function (resolve, reject) {</span></span>
<span class="line"><span style="color:#A6ACCD;">            instance[method](uri, data, config)</span></span>
<span class="line"><span style="color:#A6ACCD;">            .then(response =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">                resolve(response)</span></span>
<span class="line"><span style="color:#A6ACCD;">            })</span></span>
<span class="line"><span style="color:#A6ACCD;">            .catch(error =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">                reject(error)</span></span>
<span class="line"><span style="color:#A6ACCD;">            })</span></span>
<span class="line"><span style="color:#A6ACCD;">        })</span></span>
<span class="line"><span style="color:#A6ACCD;">    }</span></span>
<span class="line"><span style="color:#A6ACCD;">});</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">export default api;</span></span></code></pre></div><ul><li>在src/api/api.js 新建接口文件</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import api from &quot;./axios&quot;;</span></span>
<span class="line"><span style="color:#A6ACCD;">// 排行榜</span></span>
<span class="line"><span style="color:#A6ACCD;">const toplist = () =&gt; { return api.get(&#39;/toplist&#39;, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">const topListDetail = () =&gt; { return api.get(&#39;/toplist/detail&#39;, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">const listDetail = ({ id = &#39;&#39;, s = 8 }) =&gt; { return api.get(\`/playlist/detail?id=\${id}&amp;s=\${s}\`, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">// 获取热门歌手</span></span>
<span class="line"><span style="color:#A6ACCD;">const artists = ({ id = &#39;&#39; }) =&gt; { return api.get(\`/artists?id=\${id}\`, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">const topArtists = ({ limit = 30, offset = 0 }) =&gt; { return api.get(\`/artists?limit=\${limit}&amp;offset=\${offset}\`, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">const artistList = ({ type = -1, area = -1, initial = &#39;&#39;, limit = 50, offset = 0 }) =&gt; { return api.get(\`/artist/list?type=\${type}&amp;area=\${area}&amp;initial=\${initial}&amp;limit=\${limit}&amp;offset=\${offset}\`, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">// 获取歌单</span></span>
<span class="line"><span style="color:#A6ACCD;">const getMusicList = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  return api.get(\`/top/playlist?limit=10&amp;order=hot&amp;cat=&amp;offset=0\`, {})</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">// 轮播图</span></span>
<span class="line"><span style="color:#A6ACCD;">const banner = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  return api.get(\`/banner?type=2\`, {})</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">// 登录接口</span></span>
<span class="line"><span style="color:#A6ACCD;">const login = (phone = &#39;&#39;, captcha = &#39;&#39;, realIP = &#39;43.241.243.255&#39;) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  return api.post(\`/login/cellphone\`, {</span></span>
<span class="line"><span style="color:#A6ACCD;">    phone,</span></span>
<span class="line"><span style="color:#A6ACCD;">    captcha: captcha,</span></span>
<span class="line"><span style="color:#A6ACCD;">    realIP</span></span>
<span class="line"><span style="color:#A6ACCD;">  })</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">// 发送验证码</span></span>
<span class="line"><span style="color:#A6ACCD;">const yzm = (phone = &#39;&#39;) =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  return api.get(\`/captcha/sent?phone=\${phone}\`, {})</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">// 退出登录</span></span>
<span class="line"><span style="color:#A6ACCD;">const logout = () =&gt; {</span></span>
<span class="line"><span style="color:#A6ACCD;">  return api.get(\`/logout\`, {})</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span>
<span class="line"><span style="color:#A6ACCD;">// 歌词</span></span>
<span class="line"><span style="color:#A6ACCD;">const lyrics = ({ id = &#39;&#39; }) =&gt; { return api.get(\`/lyric?id=\${id}\`, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">// 热门搜索</span></span>
<span class="line"><span style="color:#A6ACCD;">const cloudsearch = ({ keywords = &#39;&#39;, limit = 30, offset = 0, type = &#39;1&#39; }) =&gt; { return api.get(\`/cloudsearch?keywords=\${keywords}&amp;limit=\${limit}&amp;offset=\${offset}&amp;type=\${type}\`, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">const serachHot = () =&gt; { return api.get(&#39;/search/hot&#39;, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">// 搜索</span></span>
<span class="line"><span style="color:#A6ACCD;">const search = ({ keywords = &#39;&#39;, limit = 30, offset = 0 }) =&gt; { return api.get(\`/search?keywords=\${keywords}&amp;limit=\${limit}&amp;offset=\${offset}\`, {}) }</span></span>
<span class="line"><span style="color:#A6ACCD;">export {</span></span>
<span class="line"><span style="color:#A6ACCD;">  login,</span></span>
<span class="line"><span style="color:#A6ACCD;">  logout,</span></span>
<span class="line"><span style="color:#A6ACCD;">  yzm,</span></span>
<span class="line"><span style="color:#A6ACCD;">  banner,</span></span>
<span class="line"><span style="color:#A6ACCD;">  getMusicList,</span></span>
<span class="line"><span style="color:#A6ACCD;">  topArtists,</span></span>
<span class="line"><span style="color:#A6ACCD;">  artistList,</span></span>
<span class="line"><span style="color:#A6ACCD;">  toplist,</span></span>
<span class="line"><span style="color:#A6ACCD;">  topListDetail,</span></span>
<span class="line"><span style="color:#A6ACCD;">  listDetail,</span></span>
<span class="line"><span style="color:#A6ACCD;">  artists,</span></span>
<span class="line"><span style="color:#A6ACCD;">  serachHot,</span></span>
<span class="line"><span style="color:#A6ACCD;">  search,</span></span>
<span class="line"><span style="color:#A6ACCD;">  cloudsearch,</span></span>
<span class="line"><span style="color:#A6ACCD;">  lyrics</span></span>
<span class="line"><span style="color:#A6ACCD;">}</span></span></code></pre></div><ol start="3"><li>在main.js 引入全局调用封装接口</li></ol><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">import { createApp } from &#39;vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import &#39;./style.css&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import App from &#39;./App.vue&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import router from &#39;./router&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">import store from &#39;./store&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;">// import { Button } from &#39;vant&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">// createApp(App).mount(&#39;#app&#39;)</span></span>
<span class="line"><span style="color:#A6ACCD;">import { Cell, CellGroup } from &#39;vant&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;">import * as getApi from &#39;@/api/api&#39;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">const app = createApp(App)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(router)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(store)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(Cell);</span></span>
<span class="line"><span style="color:#A6ACCD;">app.use(CellGroup);</span></span>
<span class="line"><span style="color:#A6ACCD;">app.config.globalProperties[&#39;$http&#39;] = getApi;</span></span>
<span class="line"><span style="color:#A6ACCD;">// app.use(Button)</span></span>
<span class="line"><span style="color:#A6ACCD;">app.mount(&#39;#app&#39;)</span></span></code></pre></div><ul><li>调用方法 (举个栗子)</li></ul><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;script setup&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"> import { reactive, getCurrentInstance } from &#39;vue&#39;;</span></span>
<span class="line"><span style="color:#A6ACCD;"> const state = reactive({</span></span>
<span class="line"><span style="color:#A6ACCD;">  phone: &#39;&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;">  captcha: &#39;&#39;,</span></span>
<span class="line"><span style="color:#A6ACCD;"> })</span></span>
<span class="line"><span style="color:#A6ACCD;"> const { proxy } = getCurrentInstance();</span></span>
<span class="line"><span style="color:#A6ACCD;"> const { data: res } = proxy.$http.login(state.phone, state.captcha);</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/script&gt;</span></span></code></pre></div><h2 id="集成-css-预编译器-sass" tabindex="-1">集成 CSS 预编译器 Sass <a class="header-anchor" href="#集成-css-预编译器-sass" aria-label="Permalink to &quot;集成 CSS 预编译器 Sass&quot;">​</a></h2><ol><li>安装</li></ol><blockquote><p>npm i sass -D</p></blockquote><ol start="2"><li>使用</li></ol><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">&lt;style lang=&quot;scss&quot;&gt;</span></span>
<span class="line"><span style="color:#A6ACCD;"></span></span>
<span class="line"><span style="color:#A6ACCD;">&lt;/style&gt;</span></span></code></pre></div>`,67),c=[t],D=JSON.parse('{"title":"Vite + Vue3 + vant 前端工程化项目环境","description":"","frontmatter":{},"headers":[],"relativePath":"vue3/construct.md","filePath":"vue3/construct.md"}'),r={name:"vue3/construct.md"},u=Object.assign(r,{setup(i){return(C,A)=>(s(),n("div",null,c))}});export{D as __pageData,u as default};
