import { mdPlugin } from './config/plugins.js'
import { defineConfig } from 'vitepress'


export default defineConfig(
  {
    base: '/vitepress/',
    head: [
      [
        "link",
        { rel: "stylesheet", href: "https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" }
      ],
      ["script", { src: "https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js" }]
    ],
    title: 'yangshu notes',
    description: 'Just playing around.',
    markdown: {
      config: (md) => {
        mdPlugin(md)
      }
    },
    themeConfig: {
      nav: [
        { text: '指南', link: '/guide/textTransform' },
      ],
      sidebar: [
        {
          text: '杨树',
          items: [
            { text: '主页', link: '/yangshu/index' },
            { text: '常用网址导航', link: '/yangshu/links' },
            { text: 'iframe的用法和a标签的target', link: '/yangshu/iframe' },
            { text: 'host文件使用方法', link: '/yangshu/host' },
            { text: '使用PHPenv软件添加站点', link: '/yangshu/phpenv' },
            { text: 'cnpm使用方式', link: '/yangshu/cnpm' },
            { text: 'aardio', link: '/aardio/index' },
            { text: '笔记', link: '/notes/index' },
            { text: 'PHP', link: '/php/index' },
          ]
        }, {
          text: 'CSS基础',
          items: [
            { text: '文本转换', link: '/guide/textTransform' },
            { text: '文字间距', link: '/guide/textSpacing' },
            { text: '文本阴影', link: '/guide/textShadow' },
            { text: '列表', link: '/guide/list' }
          ]
        },
        {
          text: 'CSS中级',
          items: [
            { text: '图像精灵', link: '/guide/imageSprite' },
            { text: '属性选择器', link: '/guide/selector' }
          ]
        },
        {
          text: 'CSS高级',
          items: [
            { text: '变量', link: '/guide/variable' },
            { text: 'Box Sizing', link: '/guide/boxSizing' },
            { text: 'Flexbox', link: '/guide/flexbox' },
            { text: '媒体查询', link: '/guide/media' }
          ]
        },
        {
          text: '前端项目环境搭建',
          items: [
            { text: 'vue3+vite+vant 项目构建', link: '/vue3/construct' },
            { text: 'vue3 与 vue2 的区别', link: '/vue3/index2' },
            { text: 'hook函数', link: '/vue3/hook' },
            { text: 'Proxy', link: '/vue3/proxy' },
            { text: 'JavaScript异步机制详解', link: '/vue3/mechanism' },
          ]
        },
      ]
    }
  }
);


/*
npm run  docs:build
git add .
git commit -m "."
git push origin master
msedge https://gitee.com/yangshu1998/vitepress/pages
*/ 