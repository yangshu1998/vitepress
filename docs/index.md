---
layout: home

hero:
  name: yangshu的笔记站点
  text: 分享web知识内容，数学，随笔
  tagline: HTML , CSS , JS , MATH ……
  actions:
    - theme: brand
      text: 开始
      link: /guide/textTransform

features:
  - icon: ⚡️
    title: Function description
    details: 渐进式 JavaScript 框架
  - icon: 🖖
    title: Function description
    details: 易学易用，性能出色，适用场景丰富的 Web 前端框架
  - icon: 🛠️
    title: Function description
    details: 组合式api
---