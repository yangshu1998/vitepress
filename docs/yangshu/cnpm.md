# npmmirror 镜像站
## 安装INSTALL
- 这是一个完整 npmjs.org 镜像，你可以用此代替官方版本(只读)，同步频率目前为 10分钟 一次以保证尽量与官方服务同步。

- 你可以使用我们定制的 cnpm (gzip 压缩支持) 命令行工具代替默认的 npm:
```bash
npm install -g cnpm --registry=https://registry.npmmirror.com
```

## 其它命令
- 支持 npm 除了 publish 之外的所有命令, 如:
```bash
cnpm info express
```
> 现在还不支持latex代码

