# 常用网址导航
## 搜索引擎
- [百度](https://www.baidu.com/)
- [知乎](https://www.zhihu.com/)
- [哔哩哔哩](https://www.baidu.com/)
- [淘宝](https://www.taobao.com/)
- [京东](https://www.jd.com/)

## 前端技术
- [Bootcss](https://www.bootcss.com/)
- [Vue.js](https://cn.vuejs.org/)


## 后台管理
- [BuildAdmin : 使用流行技术栈快速创建商业级后台管理系统](https://wonderful-code.gitee.io/)
- [Fastadmin : 模块化开发 , 后台管理低代码](https://www.fastadmin.net/)

## GUI开发

- [Niva 轻松构建超轻量级跨平台应用，Niva 让开发变得简单！](https://bramblex.github.io/niva/)

- [Electron是一个使用 JavaScript、HTML 和 CSS 构建桌面应用程序的框架。](https://www.electronjs.org/zh/docs/latest)