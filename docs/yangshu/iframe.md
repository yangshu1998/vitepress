# iframe的用法和a标签的target

## 页面结构

```html
    <div class="container">
        <div class="left-nav">
            <ul>
                <li><a target="frame" href="https://cloud.tencent.com/developer/ask/sof/844519/answer/1215800">腾逊开发者社区</a></li>
                <li><a target="frame" href="https://www.runoob.com/css/css-dropdowns.html">菜鸟教程</a></li>
                <li><a target="frame" href="https://zhuanlan.zhihu.com/p/573207444">知乎专栏</a></li>
                <li><a target="frame" href="http://tk.hustoj.com/">HustOJ</a></li>
            </ul>
        </div>
        <iframe class="iframe" name="frame" src="" frameborder="0"></iframe>
    </div>
```

### a标签的target指向iframe的name，就可以完成跳转

## 总体代码，实现导航栏

效果如图

![](res/127.0.0.1_5500_a.html.png)

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div class="container">
        <div class="left-nav">
            <ul>
                <li><a target="frame" href="https://cloud.tencent.com/developer/ask/sof/844519/answer/1215800">腾逊开发者社区</a></li>
                <li><a target="frame" href="https://www.runoob.com/css/css-dropdowns.html">菜鸟教程</a></li>
                <li><a target="frame" href="https://zhuanlan.zhihu.com/p/573207444">知乎专栏</a></li>
                <li><a target="frame" href="http://tk.hustoj.com/">HustOJ</a></li>
            </ul>
        </div>
        <iframe class="iframe" name="frame" src="" frameborder="0"></iframe>
    </div>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        .container {
            width: 100vw;
            height: 100vh;
            display: flex;
            overflow: hidden;
            flex-direction: row;
        }

        .left-nav {
            width: 200px;
            background-color: #00965e1a;
            height: 100%;
            /* background-color: antiquewhite; */
        }

        .iframe {
            /* background-color: beige; */
            flex: 1;
            height: 100%;
        }
        .left-nav ul{
            width: 100%;
        }

        .left-nav ul li{
            width: 180px;
            padding: 10px;
            height: 30px;
            /* background-color: #00965e1a; */
            text-align: center;
        }

        .left-nav ul li a:hover{
            color: white;
            background-color: #198754;
        }
        .left-nav ul li a{
            width: 100%;
            display: block;
            height: 100%;
            text-decoration: none;
            color: black;
            line-height: 30px;
            
        }
    </style>
</body>

</html>
```