# proxy -- Object.defineProperty() Vue双向绑定的原理

## Object.defineProperty()

在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回这个对象
(拦截的是对象的属性，会改变原有的对象)

## 语法

```
Object.defineProperty(obj, prop, descriptor)
```

## 参数

* `obj`: 要添加对象的属性

* `prop`: 要定义或修改属性的名称

* `descriptor`: 要定义或修改属性描述符

## 使用方法

* 基本使用方法

* 监听对象上的多个属性

* 深度监听对象

```
<script setup>
const person = {
    name: '小白',
    age: 18,
    object: {
      value: 1
    }
}

// 实现一个响应式函数
function defineProperty(obj, key, val) {
    // 如果对象中的属性也是一个对象时，递归进入对象，进行监听
    if (typeof val === 'object') {
        Observer(val)
    }
    Object.defineProperty(obj, key, {
        get() {
            console.log(`读取: ${key}属性`)
            return val
        },
        set(newVal) {
            // 如果我们对对象中的某个属性值，从字符串更改成对象，我们应该如何监听新添加对象的属性值呢?
            if (typeof newVal === 'object') {
                Observer(key)
            }
            console.log(`修改${key}。值为: ${newVal}`)
            val = newVal
        }
    })
}
// 实现一个遍历函数Observer
// 我们通常在实际情况中，需要监听对象中多个属性的变化
// 这个时候我们需要配合Object.keys()进行遍历
function Observer(obj) {
    // 如果传入的值不是一个对象时候，return
    if (typeof obj !== 'object' || obj === null) {
        return
    }
    Object.keys(obj).forEach((key) => {
        defineProperty(obj, key, obj[key])
    })
}

Observer(person)
console.log(person.name)
console.log(person.name = '小黑')
console.log(person.object.value = '2')

</script>

```

## 总结

通过以上对es5的`Object.defineProperty()`了解中，其实我们还有问题没有解决。
当我们要给对象新增属性时候，我们也需要手动去监听这个新增属性

> `Object.defineProperty`无法监控到数组下标的变化，导致直接通过数组的下标给数组设置值，不能实时响应
>
> 为了解决这个问题，vue内部经过处理后可以使用几种方法来监听数组`push()`, `pop()`, `shift()`, `unshift()`, `splice()`, `sort()`, `reverse()`
>
> 正是因为这个原因，在使用vue给`data`中的数组或者对象新增属性时候，需要使用vm.$set去更新视图，才能保证新增的属性属于响应式的
>
> 可以看到，通过`Object.definePorperty()`进行数据监听是比较麻烦的，需要大量的手动处理.继而在vue3中转而采用`Proxy`

## Vue3响应式数据原理 --- Proxy

## 介绍

Proxy 对象用于创建一个对象的代理，从而实现基本操作的拦截和自定义（如属性查找，赋值，枚举，函数调用等）

## 语法

```
const p = new Proxy(target, handler)
```

## 参数

`target`

要使用`Proxy`包装的目标对象（可以是任何类型的对象，包括原生数组，函数，甚至另一个代理）

`handler`

一个通常以函数作为属性的对象，各属性中的函数分别定义了在执行各种操作时代理`p`的行为

## 使用方法

```
<script setup>
const person = {
    name: '小白',
    age: 18
}

const proxy = new Proxy(person, {
    // target代表源数据
   // propName代表操作的那个属性名
    get(target, propName) {
        console.log(`读取proxy: ${propName}属性`)
        return target[propName]
    },
    set(target, propName, value) {
        console.log(`修改proxy: ${propName}属性。值为: ${value}`)
        return target[propName] = value
    },
    deleteProperty(target, propName) {
        console.log(`删除proxy: ${propName}属性`)
        return delete target[propName]
    }
})

console.log(proxy.name)
console.log(proxy.name = '小黑')
console.log(delete proxy.name, proxy)
</script>

```

可以看出，`Proxy`代理的是整个对象，而不是对象的某个特定属性，不需要我们通过遍历来逐个进行数据绑定

> 值得注意的是：我们在使用`Object.definePorperty()`对对象添加属性的时候，我们对对象的读写操作针对还是对象本身
>
> 在`Proxy`中，如果需要进行读写操作，那么我们需要对实例对象`proxy`进行操作

## 解决Object.definePorperty()中遇到的问题

在上面使用`Object.definePorperty()`的时候，我们遇到的问题有：

* 一次只能对一个属性进行监听，需要遍历来对所有属性进行监听。（我们上面已经解决了）

* 在遇到一个对象的属性还是一个对象的情况下，需要递归监听

```
<script setup>
const person = {
    name: '小白',
    age: 18,
    object: {
        value: 1
    }
}

const proxy = new Proxy(person, {
    // target代表源数据
   // propName代表操作的那个属性名
    get(target, propName) {
        console.log(`读取proxy: ${propName}属性`)
        return target[propName]
    },
    set(target, propName, value) {
        console.log(`修改proxy: ${propName}属性。值为: ${value}`)
        return target[propName] = value
    },
    deleteProperty(target, propName) {
        console.log(`删除proxy: ${propName}属性`)
        return delete target[propName]
    }
})

// console.log(proxy.name)
// console.log(proxy.name = '小黑')
console.log(proxy.object.value = '2')
// console.log(delete proxy.name, proxy)
</script>

```

可以看到，当我们访问proxy的深层属性时候，并不会触发set。 所有proxy如果想要实现深度监听，也需要实现上文Observer的递归函数，使用proxy逐个对对象中的每个属性进行拦截

```
<script setup>
const person = {
    name: '小白',
    age: 18,
    object: {
        value: 1
    }
}

function deepProxy(obj, cb) {
    if (typeof obj === 'object') {
        ObserverProxy(obj)
    }
    return new Proxy(obj, {
        get(target, propName) {
       console.log(`读取proxy: ${propName}属性`)
       return target[propName]
        },
        set(target, propName, value) {
            if (typeof propName === 'object') {
                value = deepProxy(value, cb)
            }
       console.log(`修改proxy: ${propName}属性。值为: ${value}`)
       return target[propName] = value
        },
        deleteProperty(target, propName) {
       console.log(`删除proxy: ${propName}属性`)
       return delete target[propName]
        }
    })
}

function ObserverProxy(obj, cb) {
    for (let key in obj) {
        if (typeof obj[key] === 'object') {
            obj[key] = deepProxy(obj[key], cb)
        }
    }
}
let b = deepProxy(person, (type, data) => {
    console.log(type, data)
})
console.log(b)
console.log(b.name)
console.log(b.object.value = '2')
</script>
```

* 对于对象的新增属性，需要手动监听

访问的proxy.value就是原本对象上不存在的属性，但是我们访问它的时候，仍然们可以被get拦截到。

* 对于数组通过push、unshift方法增加的元素，也无法监听

```
<script setup>

const person = {
    name: '小白',
    age: 18,
    object: {
        value: 1
    }
}

function deepProxy(obj, cb) {
    if (typeof obj === 'object') {
        ObserverProxy(obj)
    }
    return new Proxy(obj, {
        get(target, propName) {
       console.log(`读取proxy: ${propName}属性`)
       return target[propName]
        },
        set(target, propName, value, receiver) {
            if (typeof propName === 'object') {
                value = deepProxy(value, cb)
            }
            let cbType = target[propName] === undefined ? 'create' : 'modify'
            // 排除数组修改length回调
            if (!(Array.isArray(target) && propName === 'length')) {
                cb(cbType, {target, propName, value})
            }
       console.log(`修改proxy: ${propName}属性。值为: ${value}`)
       return Reflect.set(target, propName, value, receiver)
        },
        deleteProperty(target, propName) {
       console.log(`删除proxy: ${propName}属性`)
       return delete target[propName]
        }
    })
}

function ObserverProxy(obj, cb) {
    for (let key in obj) {
        if (typeof obj[key] === 'object') {
            obj[key] = deepProxy(obj[key], cb)
        }
    }
}
let a = deepProxy(['小白'], (type, data) => {
    console.log(type, data)
})
console.log(a)
console.log(a.push('小黑'))

</script>

```
