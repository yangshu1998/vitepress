# Vue3 与 Vue2 的区别

## 生命周期

> Vue3 和 Vue2 生命周期基本相同，都可以通过配置项去使用生命周期钩子，
> 在流程上有一些许区别，一些钩子名称发生变化，并且Vue3可以通过组合式API形式
> 去使用生命组件钩子

### Vue2 生命周期图示  Vue3 生命周期图示

![生命周期](../image/5.png){data-fancybox=gallery}

1. Vue3 可以继续使用 Vue2中的生命周期钩子，但有两个被更名

* `beforeDestroy` --- `beforeUnmount`
* `destroyed`  --- `unmounted`

2. 从上图可以看出Vue3 先将`el`挂载之后在进行 `beforeCreate`，而Vue2是在 `created` 之后再进行判断是否挂载 `el`
  如果没有，则流程终止，`beforeCreate`和 `created` 俩个钩子冗余使用，因此Vue3进行了优化的

### 组合式API形式使用生命周期钩子

| Vue2 | Vue3 | |
| :--- | :--- | :--- |
| beforeCreate | setup() | |
| created | setup()  | 开始创建组件之前，在beforeCreate和created之前执行。创建的是data和method |
| beforeMount | onBeforeMount  | 组件挂载到节点上之前执行的函数 |
| mounted     | onMounted  | 组件挂载完成后执行的函数 |
| beforeUpdate   | onBeforeUpdate  | 组件更新完成之后执行的函数 |
| updated        | onUpdated  | 组件更新完成之后执行的函数 |
| beforeDestroy  | onBeforeUnmount  | 组件卸载之前执行的函数 |
| destroyed      | onUnmounted  | 组件卸载完成后执行的函数 |
| activated      | onActivated  | 被包含在`<keep-alive>`中的组件，会多出两个生命周期钩子函数。被激活时执行 |
| deactivated    | onDeactivated  | 比如从 A 组件，切换到 B 组件，A 组件消失时执行 |
| errorCaptured  | onErrorCaptured  | 当捕获一个来自子孙组件的异常时激活钩子函数 |

## 选项式API 与 组合式 API

* 选项式api

::: demo
index2/index2
:::

* 组合式api

::: demo
index2/index3
:::

* setup 语法糖

::: demo
index2/index4
:::

### 总结

* 选项式api

> vue2 的选项式api 在组织代码中，在一个vue文件中`data`,`methods`,`computed`,`watch`中定义属性和方法，共同处理页面逻辑
>
> 优点：新手上手简单
>
> 缺点：在复杂项目中，不同功能之前的方法、属性、计算属性都要对应去编写内容，查找相应内容时候还需要上下翻滚，影响效率

![https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e256700683674f71978c9dc13e64ae8e~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/e256700683674f71978c9dc13e64ae8e~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image)

* 组合式api

> 缺点：编写思维逻辑需要转变，学习成本增加
>
> 优点：`Composition API` 是根据逻辑相关组织相应代码 ， 可以提高相应代码可读性，复用性，基于函数组合的api更好的重用代码
>
> 在vue2中复用代码一般通过mixins 复用代码，但是容易导致命名冲突且关系不清楚
>
> `Composition API` 最大的优点可将一个功能相关的 放在同一个地方，可以将每个单独的功能放在独立的函数内，最后在组合起来

![https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/eb5fcf366480466cab33f3efa7808a9a~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/eb5fcf366480466cab33f3efa7808a9a~tplv-k3u1fbpfcp-zoom-in-crop-mark:4536:0:0:0.image)

## 声明响应式状态（ref 和 reactive）

>在vue2中选项式api中，data中的函数都具有响应式，页面会随着data的数据变化而变化，而在vue3中没有data函数，为了解决这个问题引入了`ref` 和 `reactive` 函数使得变量变成响应式数据

* ref

1.ref的参数一般是基本数据类型，也可以是对象类型

2.如果参数是对象类型，本质的底层其实还是`reactive`，系统会自动将`ref`转换成`reactive`，例如`ref(1) ==> reactive({value: 1})`

3.在模板中访问`ref`的数据时，系统会自动添加`.value`，在Js中访问`ref`的数据，我们需要手动添加`.value`

4.ref的底层原理同reactive一样，都是Proxy

* reactive

1.reactive的参数一般是对象或者数组，它能够将复杂的数据类型转变为响应式数据

2.reactive的响应式是深层次的，底层本质是将传入的数据转换成Proxy对象

## watch 和 computed

* watch函数
* computed 函数

::: demo
index2/watch
:::

## Vue3 组件通信

| 方式 | Vue2 | Vue3 |
| :--- | :--- | :--- |
| 父传子 | props | props |
| 子传父 | $emit | emits |
| 父传子 | $attrs | attrs |
| 子传父 | $listeners | 无(合并到 attrs方式) |
| 父传子 | provide | provide |
| 子传父 | inject | inject |
| 子组件访问父组件 | $parent | 无 |
| 父组件访问子组件 | $children | 无 |
| 父组件访问子组件 | $ref | expose&ref |
| 兄弟传值 | EventBus | mitt |

## props

props：父组件通过v-bind传入，子组件通过props接收

> 注意：
>
> props中的数据是单向的，即子组件不可更改父组件传过来的值
>
> 在组合式api中，如果想要在子组件中用其他变量接收props的值时需要使用toRef将props中的属性转化为响应式

## emit

emit: 子组件可以通过emit发布一个事件并传递一些参数，父组件通过v-on进行这个事件的监听

## attrs和listeners

* 子组件使用$attrs可以获得父组件除了props传递的属性和特性绑定属性 (class和 style)之外的所有属性
* 子组件使用$listeners可以获得父组件(不含.native修饰器的)所有v-on事件监听器，在Vue3中已经不再使用；
* 但是Vue3中的attrs不仅可以获得父组件传来的属性也可以获得父组件v-on事件监听器

## provide/inject

* provide: 是一个对象，或者是返回一个对象的函数，里面包含要给子孙后代的东西，也就是属性和属性值

* inject：一个字符串数组，或者是一个对象，获取父组件或者更高层次的组件provide的值，即在任何后代组件都可以通过inject获得

## expose&ref

* $refs可以直接获取元素属性，同时也可以直接获取子组件实例

> 注意：通过ref获取子组件实例必须在页面挂载完成后才能获取。
>
> 在使用setup语法糖时候，子组件必须元素或方法暴露出去父组件才能获取到

## EventBus/mitt

1. 兄弟组件通信可以通过一个事件中心EventBus实现，即新建一个Vue实例来进行事件的监听，触发和销毁

2. 在Vue3中没有了EventBus兄弟组件通信，但是现在有了一个替代的方案`mitt.js`, 原理还是EventBus

首先先安装mitt

> npm i mitt -S

新建文件src/utils/mitt.js

```
import mitt from 'mitt'
const Mitt = mitt()
export default Mitt
```

* 父组件

```
<template>
 <div>
  <Button type="primary" @click="showConfirm">弹窗</Button>
  <confirm ref="confirm" text="是否清空所有搜索历史" confirmBtnText="清空" name='confirm' @confirmClick='confirmClick'></confirm>
  <hello-word></hello-word>
 </div>
</template>

<script setup>
import { Button } from 'vant';
import Confirm from '@/base/confirm.vue';
import HelloWord from '@/components/HelloWord.vue'
import { ref, provide } from 'vue';
const confirm = ref(null);
const msg = ref('下发给子组件')
provide('msg', msg)
const showConfirm = () => {
 confirm.value.show();
};
const confirmClick = () => {
 console.log('父组件被调用')
}
</script>

<style lang="scss"></style>

```

* 子组件

```
<template>
 <div>
  <Button type="primary" @click="handleClick">调用父组件方法</Button>
  <Button type="primary" @click="handleClickSendMsg">传值</Button>
  <transition name="confirm-fade">
   <div class="confirm" v-show="showFlag">
    <div class="confirm-wrapper">
     <div class="confirm-content">
      <p class="text">{{props.text}}</p>
      <div class="operate">
       <div @click="cancel" class="operate-btn left">{{ props.cancelBtnText }}</div>
       <div @click="confirm" class="operate-btn">{{ props.confirmBtnText }}</div>
      </div>
     </div>
    </div>
   </div>
  </transition>
 </div>
</template>

<script setup>
import { ref, useAttrs, inject } from 'vue';
import { Button } from 'vant';
import Mitt from '@/utils/mitt.js'
const showFlag = ref(false);
const attrs = useAttrs()
const emit = defineEmits(['cancel', 'confirm'])
const props = defineProps({
 text: {
  type: String,
  default: ''
 },
 confirmBtnText: {
  type: String,
  default: '确定'
 },
 cancelBtnText: {
  type: String,
  default: '取消'
 }
});
const show = () => {
 showFlag.value = true
}
const hide = () => {
 showFlag.value = false
}
console.log(attrs)
console.log(inject('msg').value)
defineExpose({
 show,
 hide
})
const cancel = () => {
 hide()
 emit('cancel')
};
const confirm = () => {
 hide()
 emit('confirm')
};
const handleClick = () => {
 attrs.onConfirmClick('Child')
}
const handleClickSendMsg = () => {
 Mitt.emit('sendMsg', '兄弟的值')
}
</script>

<style lang="scss" scoped>
.confirm {
 position: fixed;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 z-index: 998;
 background-color: rgba($color: #000000, $alpha: 0.3);
 &.confirm-fade-enter-active {
  animation: confirm-fadein 0.3s;
  .confirm-content {
   animation: confirm-zoom 0.3s;
  }
 }
 .confirm-wrapper {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 999;
  .confirm-content {
   width: 270px;
   border-radius: 13px;
   background: #333;
   .text {
    padding: 19px 15px;
    line-height: 22px;
    text-align: center;
    font-size: 18px;
    color: rgba($color: #fff, $alpha: 0.5);
   }
   .operate {
    display: flex;
    align-items: center;
    text-align: center;
    font-size: 18px;
    .operate-btn {
     flex: 1;
     line-height: 22px;
     padding: 10px 0;
     border-top: 1px solid rgba($color: #000000, $alpha: 0.3);
     color: rgba($color: #fff, $alpha: 0.3);
     &.left {
      border-right: 1px solid rgba($color: #000000, $alpha: 0.3);
     }
    }
   }
  }
 }
}
@keyframes confirm-fadein {
 0% {
  opacity: 0;
 }
 100% {
  opacity: 1;
 }
}
@keyframes confirm-zoom {
 0% {
  transform: scale(0);
 }
 50% {
  transform: scale(1.1);
 }
 100% {
  transform: scale(1);
 }
}
</style>


```

* 兄弟组件

```

<template>
 <div>
   HelloWord
 </div>
</template>

<script setup>
import { onUnmounted } from 'vue'
import Mitt from '@/utils/mitt.js'
const getMsg = (val) => {
  console.log(val);//兄弟的值
}
Mitt.on('sendMsg', getMsg)
onUnmounted(() => {
 //组件销毁 移除监听
 Mitt.off('sendMsg', getMsg)
})
</script>

<style scoped>

</style>


```
