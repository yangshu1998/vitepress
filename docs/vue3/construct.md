# Vite + Vue3 + vant 前端工程化项目环境

## 技术栈

编程语言： HTML CSS3 JavaScript<br>
构建工具：Vite 2.x<br>
前端框架：Vue 3.x<br>
路由工具：Vue Router 4.x<br>
状态关联：Vuex 4.x<br>
UI框架：Vant<br>
Css预编译：sass<br>
HTTP工具：Axios<br>

## 架构搭建

本项目使用Vite构建工具，Vite 需要 Node.js 版本 14.18+，16+。然而，有些模板需要依赖更高的 Node 版本才能正常运行，当你的包管理器发出警告时，请注意升级你的 Node 版本。
本项目中的Node版本为 >= 12.0.0

## 使用 Vite 快速初始化项目雏形

* 使用npm
  > ~~npm init @vitejs/app~~ 已经废弃
  >
  > npm create vite@latest
  >
  > npm create vite@latest my-vue-app -- --template vue

然后按照终端提示完成以下操作

  1.输入项目名称, 选择模板

  ![图片1](../image/1.jpg){data-fancybox=gallery}

  2.安装依赖，启动项目

  > 注意：
  >
  >下图中因为当前node版本过低，所以会出现依赖警告（可以安装一个nvm）去切换node版本
  >[https://www.jianshu.com/p/13c0b3ca7c71 nvm安装教程](https://www.jianshu.com/p/13c0b3ca7c71 )

  ![图片1](../image/2.jpg){data-fancybox=gallery}

  ![图片1](../image/3.jpg){data-fancybox=gallery}

  ![图片1](../image/4.jpg){data-fancybox=gallery}

  如上图，表示 Vite + Vue3 简单的项目骨架搭建完毕，下面我们来为这个项目集成 Vue Router、Vuex、Vant、Axios、Sass。

## 规范目录结构

```
├─index.html
├─package-lock.json
├─package.json
├─README.md
├─vite.config.js   // Vite 配置文件
├─src
|  ├─App.vue
|  ├─main.js
|  ├─style.css
|  ├─views  //页面组件目录
|  ├─utils  //工具函数目录
|  ├─store  //状态管理目录
|  ├─router //路由配置目录
|  ├─components   //公共组件目录
|  ├─base         //通用类库目录
|  ├─assets       //静态资源目录
|  ├─api          //接口资源目录
├─public
├─.vscode
```

## 修改 Vite 配置文件

本内容讲解配置先做简单配置：

```
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -> npm i @types/node -D
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
 resolve: {
  alias: {
   '@': resolve(__dirname, 'src') // 设置 `@` 指向 `src` 目录
  }
 },
 base: '/', //设置打包路径
 server: {
  open: true, //启动自动打开浏览器
  hmr: true, //开启热更新
  port: '80', //指定开发服务器端口
  host: '0.0.0.0', //指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址
  cors: true, //为开发服务器配置 CORS
  proxy: {  //// 设置代理，根据我们项目实际情况配置
   "/api": {
    target: '',
    ws: true,
    changeOrigin: true,
    rewrite: (path) => path.replace(/^\/api/, '')
   }
  }
 }
})
```

## 配置路由工具 Vue Router

1.安装支持 Vue3 的路由工具 vue-router@4
> npm i vue-router@4

2.创建相关文件

* src/router/index.js 文件
* components/recommend/recommend.vue 文件

```
import { createRouter, createWebHashHistory } from "vue-router";
import Recommend from '@/components/recommend/recommend.vue';
// 懒加载方式
// const Recommend = () => import('@/components/recommend/recommend.vue')
const routes = [
 {
  path: '/',
  name: 'Recommend',
  component: Recommend,
 },
]
const router = createRouter({
  history: createWebHashHistory(),
  routes
})
export default router
```

3.在main.js文件中挂载路由, App.vue 编写 `<router-view></router-view>`

```
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'

// createApp(App).mount('#app')
const app = createApp(App)
app.use(router)
app.mount('#app')
```

## 配置状态管理工具 Vuex

1.安装支持 Vue3 的状态管理工具 vuex@next
> npm i vuex@next

2.创建相关文件

* src/store/index.js 文件
* src/store/modules/storeState.js 文件
* src/store/actions.js
* src/store/getter.js

3.在 main.js 文件中挂载 Vuex 配置

```
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import store from './store'

// createApp(App).mount('#app')
const app = createApp(App)
app.use(router)
app.use(store)
app.mount('#app')
```

## 集成 UI 框架 Vant

1.在 vite 项目中按需引入组件

2.在 vite 项目中使用 Vant 时，推荐安装 `vite-plugin-style-import` 插件，它可以自动按需引入组件的样式。

> npm i vant
>
> npm i vite-plugin-style-import@1.4.1 -D

3.安装完成后，在 vite.config.js 文件中配置插件：

```
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -> npm i @types/node -D
import styleImport, { VantResolve } from 'vite-plugin-style-import';
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
  vue(),
  styleImport({
   resolves: [VantResolve()],
   // 按照官网配置会报错，此处需添加libs代码，引入的样式多了lib
   libs: [
    {
     libraryName: 'vant',
     esModule: true,
     resolveStyle: name => `../es/${name}/style`
    }
   ]
  })
 ],
 resolve: {
  alias: {
   '@': resolve(__dirname, 'src') // 设置 `@` 指向 `src` 目录
  }
 },
 base: '/', //设置打包路径
 server: {
  open: true, //启动自动打开浏览器
  hmr: true, //开启热更新
  port: '80', //指定开发服务器端口
  host: '0.0.0.0', //指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址
  cors: true, //为开发服务器配置 CORS
  proxy: {  //// 设置代理，根据我们项目实际情况配置
   "/api": {
    target: '',
    ws: true,
    changeOrigin: true,
    rewrite: (path) => path.replace(/^\/api/, '')
   }
  }
 }
})

```

4.配置基于 Viewport 的适配方案

1. 安装`postcss-px-to-viewport-8-plugin`

> npm i postcss-px-to-viewport-8-plugin

2. 安装完成后，在 vite.config.js 文件中配置插件：

```
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
// 如果编辑器提示 path 模块找不到，则可以安装一下 @types/node -> npm i @types/node -D
import styleImport, { VantResolve } from 'vite-plugin-style-import';

import postcsspxtoviewport8plugin from 'postcss-px-to-viewport-8-plugin'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
  vue(),
  styleImport({
   resolves: [VantResolve()],
   // 按照官网配置会报错，此处需添加libs代码，引入的样式多了lib
   libs: [
    {
     libraryName: 'vant',
     esModule: true,
     resolveStyle: name => `../es/${name}/style`
    }
   ]
  })
 ],
 resolve: {
  alias: {
   '@': resolve(__dirname, 'src') // 设置 `@` 指向 `src` 目录
  }
 },
 base: '/', //设置打包路径
 server: {
  open: true, //启动自动打开浏览器
  hmr: true, //开启热更新
  port: '80', //指定开发服务器端口
  host: '0.0.0.0', //指定服务器应该监听哪个 IP 地址。 如果将此设置为 0.0.0.0 或者 true 将监听所有地址，包括局域网和公网地址
  cors: true, //为开发服务器配置 CORS
  proxy: {  //// 设置代理，根据我们项目实际情况配置
   "/api": {
    target: '',
    ws: true,
    changeOrigin: true,
    rewrite: (path) => path.replace(/^\/api/, '')
   }
  }
 },
 css: {
  postcss: {
   plugins: [
    postcsspxtoviewport8plugin({
     unitToConvert: 'px', // 要转化的单位
     viewportWidth: 375, // UI设计稿的宽度
     unitPrecision: 6, // 转换后的精度，即小数点位数
     propList: ['*'], // 指定转换的css属性的单位，*代表全部css属性的单位都进行转换
     viewportUnit: 'vw', // 指定需要转换成的视窗单位，默认vw
     fontViewportUnit: 'vw', // 指定字体需要转换成的视窗单位，默认vw
     selectorBlackList: ['ignore-'], // 指定不转换为视窗单位的类名，
     minPixelValue: 1, // 默认值1，小于或等于1px则不进行转换
     mediaQuery: true, // 是否在媒体查询的css代码中也进行转换，默认false
     replace: true, // 是否转换后直接更换属性值
     // exclude: [/node_modules/], // 设置忽略文件，用正则做目录名匹配
     exclude: [],
     landscape: false // 是否处理横屏情况
    })
   ]
  }
 }
})

```

3. 配置完成后,可在main.js 直接使用组件或者在单页面中调用组件

```
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import store from './store'
import { Button } from 'vant';
// createApp(App).mount('#app')
const app = createApp(App)
app.use(router)
app.use(store)
app.use(Button)
app.mount('#app')

```

```
<template>
 <h1>helloword</h1>
 <Button type="primary">主要按钮</Button>
 <cell-group>
   <cell title="单元格" value="内容" />
   <cell title="单元格" value="内容" label="描述信息" />
 </cell-group>
</template>

<script setup>
 import { Button, Cell, CellGroup  } from 'vant';
</script>

<style>
 h1 {
  width: 100px;
 }
</style>
```

## 集成 HTTP 工具 Axios

1. 安装Axios

> npm i axios

2. 配置axios

* 在src/api/axios.js 新建配置文件

```
import Axios from 'axios';
let baseURL = '';
if (import.meta.env.MODE  === 'development') {
  baseURL = '/api'
} else if (import.meta.env.MODE === 'production') {
  baseURL = import.meta.env.VITE_BASE_URL
}
console.log(baseURL)
/*  防止请求重复
1. 我们需要对所有正在进行中的请求进行缓存。在请求发起前判断缓存列表中该请求是否正在进行，如果有则取消本次请求。
2.在任意请求完成后，需要在缓存列表中删除该次请求，以便可以重新发送该请求
*/

//正在请求的API队列
let requestList = []

/**
 * @name:  阻止请求
 * @param {array} requestList 当前API请求队列
 * @param {string} currentUrl  当前请求API
 * @param {function} cancelFn  请求中断函数
 * @param {string} errorMsg   中断错误信息
 */
 const stopRepeatRequest = (requestList, currentUrl, cancelFn, errorMsg) => {
  const errorMessage = errorMsg || '请求出错拥堵'
  for (let i = 0; i < requestList.length; i++) {
    if (requestList[i] === currentUrl) {
      cancelFn(errorMessage)
      return
    }
  }
  // 将当前请求加入执行队列
  requestList.push(currentUrl)
}

/**
 * @name:  请求完成后从队列删除当前请求
 * @param {array} requestList 当前API请求队列
 * @param {string} currentUrl  当前请求API
 */
 const allowRequest = (requestList, currentUrl) => {
  for (let i = 0; i < requestList.length; i++) {
    if (requestList[i] === currentUrl) {
      requestList.splice(i, 1)
      break
    }
  }
}

const instance = Axios.create({
  baseURL,
  timeout: 2000,
  headers: {
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  }
})

// 前置拦截器(发起请求前拦截)
instance.interceptors.request.use((config) => {
  // 这里根据你项目实际情况来对config做处理
  // 设置cancelToken
  let cancelFn = null;
  config.cancelToken = new Axios.CancelToken((c) => {
    cancelFn = c;
  })
  // 阻止重复请求
  stopRepeatRequest(requestList, config.url, cancelFn, `不要连续请求：${config.url}，速度太快了`)
  if (localStorage.getItem('token')) {
    config.headers.common['Authorization'] = localStorage.getItem('token')
  }
  return config
}, (error) => {
  return Promise.reject(error)
})

// 后置拦截器(获取到响应式的拦截)
instance.interceptors.response.use((response) => {
  //不得重复发送
  setTimeout(() => {
    allowRequest(requestList, response.config.url), 1000
  })
  return response
}, (error) => {
  return Promise.reject(error)
})

// 对错误信息的处理函数
const ajaxMethod = ['get', 'post']
const api = {}
ajaxMethod.forEach(method => {
    api[method] = function (uri, data, config) {
        return new Promise(function (resolve, reject) {
            instance[method](uri, data, config)
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    }
});

export default api;
```

* 在src/api/api.js 新建接口文件

```
import api from "./axios";
// 排行榜
const toplist = () => { return api.get('/toplist', {}) }
const topListDetail = () => { return api.get('/toplist/detail', {}) }
const listDetail = ({ id = '', s = 8 }) => { return api.get(`/playlist/detail?id=${id}&s=${s}`, {}) }
// 获取热门歌手
const artists = ({ id = '' }) => { return api.get(`/artists?id=${id}`, {}) }
const topArtists = ({ limit = 30, offset = 0 }) => { return api.get(`/artists?limit=${limit}&offset=${offset}`, {}) }
const artistList = ({ type = -1, area = -1, initial = '', limit = 50, offset = 0 }) => { return api.get(`/artist/list?type=${type}&area=${area}&initial=${initial}&limit=${limit}&offset=${offset}`, {}) }
// 获取歌单
const getMusicList = () => {
  return api.get(`/top/playlist?limit=10&order=hot&cat=&offset=0`, {})
}
// 轮播图
const banner = () => {
  return api.get(`/banner?type=2`, {})
}
// 登录接口
const login = (phone = '', captcha = '', realIP = '43.241.243.255') => {
  return api.post(`/login/cellphone`, {
    phone,
    captcha: captcha,
    realIP
  })
}
// 发送验证码
const yzm = (phone = '') => {
  return api.get(`/captcha/sent?phone=${phone}`, {})
}
// 退出登录
const logout = () => {
  return api.get(`/logout`, {})
}
// 歌词
const lyrics = ({ id = '' }) => { return api.get(`/lyric?id=${id}`, {}) }
// 热门搜索
const cloudsearch = ({ keywords = '', limit = 30, offset = 0, type = '1' }) => { return api.get(`/cloudsearch?keywords=${keywords}&limit=${limit}&offset=${offset}&type=${type}`, {}) }
const serachHot = () => { return api.get('/search/hot', {}) }
// 搜索
const search = ({ keywords = '', limit = 30, offset = 0 }) => { return api.get(`/search?keywords=${keywords}&limit=${limit}&offset=${offset}`, {}) }
export {
  login,
  logout,
  yzm,
  banner,
  getMusicList,
  topArtists,
  artistList,
  toplist,
  topListDetail,
  listDetail,
  artists,
  serachHot,
  search,
  cloudsearch,
  lyrics
}
```

3. 在main.js 引入全局调用封装接口

```
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import store from './store'
// import { Button } from 'vant';
// createApp(App).mount('#app')
import { Cell, CellGroup } from 'vant';
import * as getApi from '@/api/api'

const app = createApp(App)
app.use(router)
app.use(store)
app.use(Cell);
app.use(CellGroup);
app.config.globalProperties['$http'] = getApi;
// app.use(Button)
app.mount('#app')

```

* 调用方法 (举个栗子)

```
<script setup>
 import { reactive, getCurrentInstance } from 'vue';
 const state = reactive({
  phone: '',
  captcha: '',
 })
 const { proxy } = getCurrentInstance();
 const { data: res } = proxy.$http.login(state.phone, state.captcha);

</script>
```

## 集成 CSS 预编译器 Sass

1. 安装

> npm i sass -D

2. 使用

```
<style lang="scss">

</style>
```
