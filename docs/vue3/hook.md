# hook函数（组合式函数）

## 介绍

* 在vue应用中，`组合式函数`是一个利用vue的组合式api来封装和复用`有状态逻辑`的函数

* vue3的hook函数，相当于vue2的mixin，不同于在于hooks是函数

* vue3的hook函数，可以帮助我们提高代码的复用性，让我们在不同的组件中都利用hooks函数

## 分页获取列表实例

我们直接在组件中使用组合式api实现获取分页列表功能,

```

<script setup>
import { ref, getCurrentInstance, onMounted } from 'vue'
const { proxy } = getCurrentInstance()
const list = ref([])
const getMusicList = async () => {
    const { data: res } = await proxy.$http.getMusicList()
    if (res.code == 200) {
        list.value = res.playlists
    }
}
onMounted(() => {
    getMusicList()
})
</script>

```

如果我们有多个list页面（接口返回格式要相同），我们要如何去复用这种相同的逻辑呢？我们可以把这个逻辑以一个组合式函数的形式提到外部文件：

```
<!-- useList.js -->
import { ref } from 'vue'

const useList = (listReq, data) => {
    if (!listReq) {
        return new Error('请传入接口调用方法')
    }
    // 判断第二个参数是否是对象，以免后面使用展开运算符时报错
    if (data && Object.prototype.toString.call(data) !== '[object Object]') {
        return new Error('额外参数请使用对象传入')
    }
    const list = ref([])
    // 新增pageInfo对象保存分页数据
    const pageInfo = ref({
        pageNum: 1,
        pageSize: 10,
        total: 0
    })
    const getList = () => {
        const params = {
            ...pageInfo.value,
            ...data
        }
        listReq(params).then((res) => {
            if (params.pageNum === 1) {
                list.value = res.data.playlists
            } else {
                list.value = [...list.value, ...res.data.playlists]
            }
            // 更新总数量
            params.total = res.data.total
        })
    }
    // 新增加载下一页的函数
    const loadMore = () => {
        pageInfo.value.pageNum += 1
        getList()
    }
    // 新增初始化
    const initList = () => {
        // 初始化一般重置所有初始条件，
        pageInfo.value.pageNum = 1
        getList()
    }
    getList()
    return {
        list,
        getList,
        loadMore,
        initList
    }
}
// const { list: goodsList, getList: getGoodsList } = useList(
//   axios.get('/url/get/goods')
// )
// const { list: recommendList, getList: getRecommendList } = useList(
//   axios.get('/url/get/goods')
// )
export default useList

```
下面是他组件中的使用方式

```
<script setup>
import { getCurrentInstance } from 'vue'
import useList from '@/hooks/useList.js'
const { proxy } = getCurrentInstance()
const { list } = useList(proxy.$http.getMusicList, {})
</script>
```
综上所述：核心逻辑完全一致，我们做的只是把它移动到另外一个外部函数中去，并返回需要暴露的状态，和在组件中一样，你也可以在组合式函数中使用所有的`组合式api`


## 约定和最佳实践

### 命名

组合式函数约定驼峰命名法命名，并以`use`作为开头

### 使用限制

组合式函数在`<script setup></script>`或`setup()`钩子中，应始终被`同步`调用，在某些场景下，你也可以在像`onMounted()`这样的生命周期钩子中使用他们

这个限制是为了让 Vue 能够确定当前正在被执行的到底是哪个组件实例，只有能确认当前组件实例，才能够：

* 将生命周期钩子注册到该组件实例上

* 将计算属性和监听器注册到该组件实例上，以便在该组件被卸载时停止监听，避免内存泄漏。

> TIP
>
> `<script setup>` 是唯一在调用 `await `之后仍可调用组合式函数的地方。编译器会在异步操作之后自动为你恢复当前的组件实例。

## 与其他模式的比较

### 和Mixin的对比

Vue 2 的用户可能会对 `mixins` 选项比较熟悉。它也让我们能够把组件逻辑提取到可复用的单元里。然而 `mixins` 有三个主要的短板：

* `不清晰的数据来源`: 当使用了多个 mixin 时，实例上的数据属性来自哪个 mixin 变得不清晰，这使追溯实现和理解组件行为变得困难。这也是我们推荐在组合式函数中使用 ref + 解构模式的理由：让属性的来源在消费组件时一目了然

* `命名空间冲突`: 多个来自不同作者的 mixin 可能会注册相同的属性名，造成命名冲突。若使用组合式函数，你可以通过在解构变量时对变量进行重命名来避免相同的键名。

* `隐式的跨 mixin 交流`: 多个 mixin 需要依赖共享的属性名来进行相互作用，这使得它们隐性地耦合在一起。而一个组合式函数的返回值可以作为另一个组合式函数的参数被传入，像普通函数那样。

